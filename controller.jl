load("sparse.jl")

# Storage of (i,j,val) combinations.
# Use if you know that you never overwrite values or
# that you don't need fast access to specific val_{i,j}
type PutOnlyMatrix
    i::Array{Int32,1}
    j::Array{Int32,1}
    val::Array{Float64,1}
    count::Int32 # Amount of values currently stored
    # Constructor sets count=0 and initializes all vectors
    PutOnlyMatrix(size::Int) = new(zeros(Int32,size), zeros(Int32,size), zeros(Float64,size), 0)
end

function put!(M::PutOnlyMatrix,i::Int,j::Int,val::Float64)
    c = M.count + 1
    M.i[c] = int32(i)
    M.j[c] = int32(j)
    M.val[c] = val
    M.count = c
end

function putonly2full(M::PutOnlyMatrix)
    maxi = max(M.i[1:M.count])
    maxj = max(M.j[1:M.count])
    Mfull = zeros(Float64, maxi, maxj)
    for k=1:M.count
        Mfull[M.i[k],M.j[k]] = M.val[k]
    end
    return Mfull
end

function zeroController(sys::BikeSystem, bfc::BestFillCache, routing::Routing, incentives_per_payout::Array{Float,2}, t_epoch::Int, horizon::Int)
    return zeros(Float64, sys.stationcount, sys.stationcount)
end

function continuousLinearizedController(sys::BikeSystem, bfc::BestFillCache, routing::Routing, incentives_per_payout::Array{Float,2}, t_epoch::Int, horizon::Int)
    scount = sys.stationcount
    ncount = sys.neighbourcount
    inci,incj = findn(incentives_per_payout)
    
    vsize_state = scount * horizon
    vsize = vsize_state + scount * sys.neighbourcount * horizon

    terminal_penalty = 1.0
    payout_penalty = 1.0
    max_payout = 2.0
    dt = 10.0 # in minutes
    
    # The cost function depends on Qdiag, the diagonal of the penalty matrix as in f(x) = x'Qx
    # It depends on 1) size of the station and 2) arrival of customers during that slice
    Qdiag = zeros(Float64,vsize)

    # We define A, b such that 
    # x_{t+1} = x_t + arrival - departure + incentive_probability * payout <=> x_{t+1} - x_t - i_p*p = a-d
    # instead of x, we have z = x-(xmax/2) as the midpoint for each station
    max_nnzA = nnz(incentives_per_payout) * horizon + scount * ((2*horizon)-1) + scount * ncount * horizon
    A = PutOnlyMatrix(max_nnzA)
    b = zeros(Float64,vsize_state)

    # Insert necessary x_t, x_{t-1} into the equation for the state update function
    for i=1:scount*horizon
        put!(A,i,i,1.0)
    end
    for i = scount+1:scount*horizon
        put!(A,i,i-scount,-1.0)
    end

    # loop over the horizon
    for t = 1:horizon
        # 1.1) Arrival and departure
        time = int(t_epoch+(t-1)*dt*60)
        date = gmtime(time)
        if date.hour == 1
            t_epoch = t_epoch + 3600 * 5
            time = int(t_epoch+(t-1)*dt*60)
            date = gmtime(time)
        end
        
        curr_slice = current_slice(date,sys.behavior.slices)

        if isbusday(date)
            arrival_sum = sys.behavior.arrival_workday_sum[:,curr_slice] * dt
            departure_sum = sys.behavior.departure_workday_sum[:,curr_slice] * dt
        else
            arrival_sum = sys.behavior.arrival_weekend_sum[:,curr_slice] * dt
            departure_sum = sys.behavior.departure_weekend_sum[:,curr_slice] * dt
        end

        # 1.2) Effects of routing vehicles
        routingEffects = zeros(Float64, scount)
        truck, nths = findn(time .< routing.t_epoch .<= time+60*dt)
        for i = 1:length(truck)
            diff = routing.delta_bikes[truck[i],nths[i]] # taking bikes
            at = routing.stations[truck[i],nths[i]]
            routingEffects[at] += diff
        end

        # 1.3) How does the position of the best point for each station change?
        # for the 1st run, distance to the best point. from 2nd to nth run: how much has the best point moved?
        bestFillDiff = zeros(Float64, scount)
        t_5 = int(floor(date.hour*12 + date.min/5)+1)
        if date.wday == 0
            best_fill = bfc.sunday
        elseif date.wday == 5
            best_fill = bfc.friday
        elseif date.wday == 6
            best_fill = bfc.saturday
        else
            best_fill = bfc.workday
        end
        
        if t==1
            for s=1:scount
                bestFillDiff[s] = float(sys.stations[s].bikes) - best_fill[s,t_5].best_amount # positive if there are too many bikes
            end
        else
            date_before = gmtime(int(time - dt*60))
            t_5_before = int(floor(date_before.hour*12 + date_before.min/5)+1)
            if date_before.wday == 0
                best_fill_before = bfc.sunday
            elseif date_before.wday == 5
                best_fill_before = bfc.friday
            elseif date_before.wday == 6
                best_fill_before = bfc.saturday
            else
                best_fill_before = bfc.workday
            end
            for s=1:scount
                bestFillDiff[s] = -best_fill[s,t_5].best_amount + best_fill_before[s,t_5_before].best_amount # positive if there are even more bikes
            end
        end
        
        # 2) Set right hand side
        b[(t-1)*scount+1:t*scount] = -departure_sum + arrival_sum + routingEffects + bestFillDiff

        # 3) Fill A
        # Fill in A the effects of incentives and the cost portion of Qdiag
        ipos = (t-1)*scount
        jpos = vsize_state + (t-1)*scount*ncount
        
        for i=1:length(inci)
            ii = inci[i]
            jj = incj[i]
            val = -incentives_per_payout[ii,jj] * arrival_sum[int(floor(jj/scount))+1]
            put!(A,ipos+ii,jpos+jj, val)
            
            if (ii-1) * ncount+1 <= jj <= ii*ncount
                # 4.1) Fill Qdiag with cost penalty. We know that the negative effect (customers leaving) is lying on the quasi-"diagonal" of the lin_incentives
                # There, we can capture all accumulated incentive effects
                Qdiag[jpos+jj] = val * payout_penalty
                # 4.2) Add to A that no less tahn 0% and no more than 100% of customers can take an incentive
                put!(A, ipos+ii+vsize_state, jpos+jj, -incentives_per_payout[ii,jj])
            end
        end
        
        # 4.3) Fill Qdiag with midpoint deviation penalty
        for s=1:scount
            plateau_size = max(1.0, best_fill[s,t_5].plateau_end-best_fill[s,t_5].plateau_start)
            Qdiag[(t-1)*scount+s] = ( (t==horizon) ? terminal_penalty : 1.0 ) / plateau_size
        end
    end
    
    ## Run the optimizer
    result = zeros(Float64,vsize)
    mosek_qp(int32(vsize),int32(vsize_state),A.count,A.i,A.j,A.val,b,float64(max_payout),Qdiag,result)

    # Map back from neighbours to stations
    payout_matrix = zeros(Float64, scount,scount)
    for o = 1:scount
        for n = 1:ncount
            neighbourid = sys.neighbours[o,n].neighbourid
            payout_matrix[o,neighbourid] = result[vsize_state+(o-1)*ncount+n]
        end
    end
    return payout_matrix
end

function continuousLinearizedStationSizeController(sys::BikeSystem, bfc::BestFillCache, routing::Routing, incentives_per_payout::Array{Float,2}, t_epoch::Int, horizon::Int)
    scount = sys.stationcount
    ncount = sys.neighbourcount
    inci,incj = findn(incentives_per_payout)

    # every state consists of (x, \overline \gamma, \overline \delta, \overline \delta' \overline sigma, \overline \theta, \underline \gamma, \underline \delta, \underline delta' \underline \sigam, \underline \theta)'
    # that makes 11 variables per state
    # the optimization variable is made up of (x(t), p(t), rest_x(t))'
    vsize_state = scount * horizon
    vsize = vsize_state
    vsize += scount * sys.neighbourcount * horizon # price
    vsize += scount * (horizon-1) * 8 # 8 additional variables to keep the station within the fill level
    vsize += scount*2 # lin dist to the plateau

    terminal_penalty = 1.0
    payout_penalty = 50
    max_payout = 2.0
    dt = 10.0 # in minutes

    max_nnzA = nnz(incentives_per_payout) * horizon # effect of payouts
    max_nnzA += scount * ncount * horizon # sum of taken payouts must be \in [0,1]
    max_nnzA += scount * (horizon-1) # old state in the state update
    max_nnzA += scount * (horizon-1) * (1+36) # new state + station fill size conditions in state update equation
    max_nnzA += scount * 8 # dist from plateau of final condition
    #max_nnzA += scount * horizon # customers at full station go to nearest neighbor
    A = PutOnlyMatrix(max_nnzA)
    Qdiag = zeros(Float64,vsize)
    c = zeros(Float64, vsize)
    constraint_count = scount*horizon*(1+1) + (scount*horizon-1)*12 + scount*4
    b = zeros(Float64,constraint_count)
    optim_dir = zeros(Int32, constraint_count) # -1: A(i,:)x <= b; 0: A(i,:)x = b; 1: A(i,:)x >= b

    # loop over the horizon
    for t = 1:horizon
        # 1) State update equation
        # 1.1) New state = old state + changes
        for i=1:scount
            put!(A,((t-1)*scount)+i, ((t-1)*scount)+i, 1.0) # current state
        end
        if t>1
            for i=1:scount
                put!(A,((t-1)*scount)+i, ((t-2)*scount)+i, -1.0) # old state
            end
        end

        # 1.2) Arrival and departure + repositioning. Part of the right hand side
        time = int(t_epoch+(t-1)*dt*60)
        date = gmtime(time)
        if date.hour == 1
            # move forward to 6am
            t_epoch = t_epoch + 3600 * 5
            time = int(t_epoch+(t-1)*dt*60)
            date = gmtime(time)
        end
        curr_slice = current_slice(date,sys.behavior.slices)
        if isbusday(date)
            arrival_sum = sys.behavior.arrival_workday_sum[:,curr_slice] * dt
            departure_sum = sys.behavior.departure_workday_sum[:,curr_slice] * dt
        else
            arrival_sum = sys.behavior.arrival_weekend_sum[:,curr_slice] * dt
            departure_sum = sys.behavior.departure_weekend_sum[:,curr_slice] * dt
        end
        repositioning_actionss = zeros(Float64, scount)
        truck, nths = findn(time .< routing.t_epoch .<= time+60*dt)
        for i = 1:length(truck)
            diff = routing.delta_bikes[truck[i],nths[i]] # taking bikes
            at = routing.stations[truck[i],nths[i]]
            repositioning_actionss[at] += diff
        end
        b[(t-1)*scount+1:t*scount] = -departure_sum + arrival_sum + repositioning_actionss
        if t==1
            for i=1:scount
                b[(t-1)*scount+i] += sys.stations[i].bikes
            end
        end
        optim_dir[(t-1)*scount+1:t*scount] = 0
        
        ipos = (t-1)*scount
        jpos = vsize_state + (t-1)*scount*ncount
        for i=1:length(inci)
            ii = inci[i]
            jj = incj[i]
            val = -incentives_per_payout[ii,jj] * arrival_sum[int(floor(jj/scount))+1]
            put!(A,ipos+ii,jpos+jj, val)
            if (ii-1) * ncount+1 <= jj <= ii*ncount
                # 2.1) Percentage of taken incentives < 100%
                put!(A, ipos+ii+vsize_state, jpos+jj, -incentives_per_payout[ii,jj])
                # 1.3) Add the percentage of customers taking a payout to the quadratic penalty matrix.. Payout costs
                Qdiag[jpos+jj] = val * payout_penalty
            end
        end

        # 2.2) Percentage of taken incentives < 100%
        b[horizon*scount+(t-1)*scount+1:horizon*scount+t*scount] = 1.0
        optim_dir[horizon*scount+(t-1)*scount+1:horizon*scount+t*scount] = -1

        if t==horizon
            continue # dont't do the fancy penalty stuff for the last period. We penalize deviation from the plateau anyways.
        end

        # 3) Keep the station fill level within [0, f_max]
        ipos = scount*horizon*2 # A has scount*horizon*2 rows filled until here.
        ipos += scount*12*(t-1)
        jpos = scount*ncount*horizon + scount*horizon # Those were the state variables and the payout vector for every timestep
        jpos += scount*8*(t-1)
        # Every station at every t has 8 additional variables
        # 1. \overline \gamma
        # 2. \overline \delta
        # 3. \overline \delta' (|\overline \delta|)
        # 4. \overline sigma
        # 5. \underline \gamma
        # 6. \underline \delta
        # 7. \underline delta'
        # 8. \underline \sigma
        # And 12 additional constraints
        # 1. \overline \delta = (f_max-x)/2
        # 2,3. -\overline \delta' <= \overline \delta <= \overline delta'
        # 4. \overline sigma >= 0
        # 5. \overline sigma >= -\overline \gamma - \overline \delta
        # 6. \overline \gamma + \overline \delta = 0
        # 7. \underline \delta = x/2
        # 8,9. -\underline \delta' <= \underline \delta <= \underline \delta
        # 10. \underline \sigma >= 0
        # 11. \underline \sigma >= -\underline \gamma - \underline \delta
        # 12. \underline \gamma + \underline \delta = 0
        for i=1:scount
            ioffset = ipos + (i-1) * 12
            joffset = jpos + (i-1) * 8
            
            # 1. (x+2\overline\delta = f_max)
            put!(A, ioffset+1, (t-1)*scount+i, 1.0)
            put!(A, ioffset+1, joffset+2, 2.0)
            b[ioffset+1] = sys.stations[i].total
            optim_dir[ioffset+1] = 0

            # 2. -\overline \delta' -\overline \delta <= 0
            put!(A, ioffset+2, joffset+3, -1.0)
            put!(A, ioffset+2, joffset+2, -1.0)
            b[ioffset+2] = 0
            optim_dir[ioffset+2] = -1

            # 3. -\overline \delta' + \overline \delta <= 0
            put!(A, ioffset+3, joffset+3, -1.0)
            put!(A, ioffset+3, joffset+2, 1.0)
            b[ioffset+3] = 0
            optim_dir[ioffset+3] = -1

            # 4.\overline sigma >= 0
            put!(A, ioffset+4, joffset+4, 1.0)
            b[ioffset+4] = 0
            optim_dir[ioffset+4] = 1

            # 5. \overline sigma + \overline \gamma + \overline \delta >= 0
            put!(A, ioffset+5, joffset+4, 1.0)
            put!(A, ioffset+5, joffset+1, 1.0)
            put!(A, ioffset+5, joffset+2, 1.0)
            b[ioffset+5] = 0
            optim_dir[ioffset+5] = 1

            # 6. \overline \gamma + \overline \delta = 0
            put!(A, ioffset+6, joffset+1, 1.0)
            put!(A, ioffset+6, joffset+3, 1.0)
            b[ioffset+6] = 0
            optim_dir[ioffset+6] = 0

            # 7. 2*\underline \delta - x = 0
            put!(A, ioffset+7, joffset+6, 2.0)
            put!(A, ioffset+7, (t-1)*scount+i, -1.0)
            b[ioffset+7] = 0
            optim_dir[ioffset+7] = 0

            # 8. \underline \delta + \underline \delta' >= 0
            put!(A, ioffset+8, joffset+6, 1.0)
            put!(A, ioffset+8, joffset+7, 1.0)
            b[ioffset+8] = 0
            optim_dir[ioffset+8] = 1

            # 9. \underline \delta' - \underline \delta >= 0
            put!(A, ioffset+9, joffset+7, 1.0)
            put!(A, ioffset+9, joffset+6, -1.0)
            b[ioffset+9] = 0
            optim_dir[ioffset+9] = 1

            # 10. \underline \sigma >= 0
            put!(A, ioffset+10, joffset+8, 1.0)
            b[ioffset+10] = 0
            optim_dir[ioffset+10] = 1

            # 11. \underline \sigma + \underline \gamma + \underline \delta >= 0
            put!(A, ioffset+11, joffset+8, 1.0)
            put!(A, ioffset+11, joffset+5, 1.0)
            put!(A, ioffset+11, joffset+6, 1.0)
            b[ioffset+11] = 0
            optim_dir[ioffset+11] = 1

            # 12. \underline \gamma + \underline \delta = 0
            put!(A, ioffset+12, joffset+5, 1.0)
            put!(A, ioffset+12, joffset+7, 1.0)
            b[ioffset+12] = 0
            optim_dir[ioffset+12] = 0
            
            # add \overline \gamma + \overline \delta - \underline \gamma - \underline \delta to the state update
            put!(A, (t-1)*scount+i, joffset+1, -1.0) #\overline \gamma
            put!(A, (t-1)*scount+i, joffset+2, -1.0) #\overline \delta
            put!(A, (t-1)*scount+i, joffset+5, 1.0) #\underline \gamma
            put!(A, (t-1)*scount+i, joffset+6, 1.0) #\underline \delta

            # # add \overline sigma to the nearest neighbour where most customers will go to..
            #nearest_neighbour = sys.neighbours[i,1].neighbourid
            #put!(A,(t-1)*scount+nearest_neighbour, joffset+4, -1.0)

            # add costs of sigma to c (with scaling such that earlier sigma are penalized a little heavier)
            c[joffset+4] = 1.0 + (horizon+1-t)/200.0
            c[joffset+8] = 1.0 + (horizon+1-t)/200.0
        end
    end
    
    # 4) Terminal penalty condition
    # Distance to the plateau of the last station fill level
    ioffset = scount*horizon*2 + scount*12*(horizon-1)
    joffset = scount*horizon + scount*ncount*horizon + scount * (horizon-1) * 8
    t5 = t5_from_epoch(int(t_epoch+horizon*dt*60))
    date = gmtime(int(t_epoch+horizon*dt*60))
    if date.wday == 0
        best_fill = bfc.sunday
    elseif date.wday == 5
        best_fill = bfc.friday
    elseif date.wday == 6
        best_fill = bfc.saturday
    else
        best_fill = bfc.workday
    end
    for s=1:scount
        # - \underline \phi * 2 + x <= \underline f
        put!(A, ioffset+4*(s-1)+1, joffset+s, -2.0)
        put!(A, ioffset+4*(s-1)+1, scount*(horizon-1)+s, 1.0)
        b[ioffset+4*(s-1)+1] = best_fill[s,t5].plateau_start
        optim_dir[ioffset+4*(s-1)+1] = -1

        # \underline \phi * 2 + x >= \underline f
        put!(A, ioffset+4*(s-1)+2, joffset+s, 2.0)
        put!(A, ioffset+4*(s-1)+2, scount*(horizon-1)+s, 1.0)
        b[ioffset+4*(s-1)+2] = best_fill[s,t5].plateau_start
        optim_dir[ioffset+4*(s-1)+2] = 1
        
        c[joffset+s] = 1.0
        
        # - \overline \phi * 2 + x <= \overline f
        put!(A, ioffset+4*(s-1)+3, joffset+scount+s, -2.0)
        put!(A, ioffset+4*(s-1)+3, scount*(horizon-1)+s, 1.0)
        b[ioffset+4*(s-1)+3] = best_fill[s,t5].plateau_end
        optim_dir[ioffset+4*(s-1)+3] = -1

        # \overline \phi * 2 + x >= \overline f
        put!(A, ioffset+4*(s-1)+4, joffset+scount+s, 2.0)
        put!(A, ioffset+4*(s-1)+4, scount*(horizon-1)+s, 1.0)
        b[ioffset+4*(s-1)+4] = best_fill[s,t5].plateau_end
        optim_dir[ioffset+4*(s-1)+4] = 1
        
        c[joffset+scount+s] = 1.0
    end
    
    ## Run the optimizer
    result = zeros(Float64,vsize)
    mosek_qp_station_size(int32(vsize),int32(vsize_state),int32(ncount),float64(max_payout),int32(constraint_count),A.count,A.i,A.j,A.val,b,optim_dir,c,Qdiag,result)

    # Map back from neighbours to stations
    payout_matrix = zeros(Float64, scount,scount)
    for o = 1:scount
        for n = 1:ncount
            neighbourid = sys.neighbours[o,n].neighbourid
            payout_matrix[o,neighbourid] = result[vsize_state+(o-1)*ncount+n]
        end
    end
    return payout_matrix
end
