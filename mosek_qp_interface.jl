_jl_mosek_qp_interface = dlopen("interfaces/mosek_qp_interface")

function mosek_qp(vsize::Int32, vsize_state::Int32, nnzA::Int32, Ai::Array{Int32,1}, Aj::Array{Int32,1}, Aval::Array{Float64,1}, beq::Array{Float64,1}, max_payout::Float64, Qdiag::Array{Float64,1}, result::Array{Float64,1})
    return ccall(dlsym(_jl_mosek_qp_interface, :solve_pricing_qp), Int32, (Int32,Int32,Int32,Ptr{Int32},Ptr{Int32},Ptr{Float64},Ptr{Float64},Float64,Ptr{Float64},Ptr{Float64}), vsize, vsize_state, nnzA, Ai, Aj, Aval, beq, max_payout, Qdiag, result)
end

function mosek_qp_station_size(vsize::Int32, vsize_state::Int32, ncount::Int32, max_payout::Float64, constraint_count::Int32, nnzA::Int32, Ai::Array{Int32,1}, Aj::Array{Int32,1}, Aval::Array{Float64,1}, b::Array{Float64,1}, optim_dir::Array{Int32,1}, c::Array{Float64,1}, Qdiag::Array{Float64,1}, result::Array{Float64,1})
    return ccall(dlsym(_jl_mosek_qp_interface, :solve_pricing_qp_station_size), Int32, (Int32,Int32,Int32,Float64,Int32,Int32,Ptr{Int32},Ptr{Int32},Ptr{Float64},Ptr{Float64},Ptr{Int32},Ptr{Float64},Ptr{Float64},Ptr{Float64}), vsize, vsize_state, ncount, max_payout, constraint_count, nnzA, Ai, Aj, Aval, b, optim_dir, c, Qdiag, result)
end

function mosek_refinement(steps::Int32, start_loaded::Float64, nnzA::Int32, Ai::Array{Int32,1}, Aj::Array{Int32,1}, Aval::Array{Float64,1}, beq::Array{Float64,1}, c::Array{Float64,1}, result::Array{Float64,1})
    return ccall(dlsym(_jl_mosek_qp_interface, :solve_repo_refinement_linp), Int32, (Int32,Float64,Int32,Ptr{Int32},Ptr{Int32},Ptr{Float64},Ptr{Float64}, Ptr{Float64}, Ptr{Float64}), steps, start_loaded, nnzA, Ai, Aj, Aval, beq, c, result)
end
