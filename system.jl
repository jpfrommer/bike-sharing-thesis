type CustomerBehavior # the expected amount of customers to arrive/leave within one minute in the respective timeslice
    departure_workday::Array{Float32,3} # i: from which station, j: to which station, k: in which timeslice
    departure_weekend::Array{Float32,3}
    departure_workday_sum::Array{Float32,2} # sum values.. all arrivals and all departures
    departure_weekend_sum::Array{Float32,2}
    arrival_workday_sum::Array{Float32,2}
    arrival_weekend_sum::Array{Float32,2}
    slices::Int # in how many slices do we partition each day?
end

type Neighbour
    stationid::Int
    neighbourid::Int
    distance::Float
end

type Station
    bikes::Int
    free::Int
    total::Int
end

type Trip
    from::Int
    to::Int
    amount::Int
    time_remaining::Float
    already_visited::Array{Int,1} # Customers don't return to stations where they took an incentive/the station was full. This is the Tabu list
end

type Truck
    loaded_bikes::Int
end

type BikeSystem
    stationcount::Int
    stations::Vector{Station}
    behavior::CustomerBehavior # when do customers arrive?
    transition_time::Matrix{Float32} # how long does it take on average to get from station a to b
    transition_time_class::Matrix{Int} # transition times clustered for rv routing
    neighbourcount::Int # for every station, we have the N nearest neighbours
    neighbours::Matrix{Neighbour} # i:stations, j: neighbours
    depot::Int # depot for routing vehicles
    trips::Vector{Trip} # queue for bikes that are currently under way
    truckcount::Int
    trucks::Vector{Truck}
end

type SystemStats
    time::Int
    total_customers::Int
    stationfull::Vector{Int}
    stationempty::Vector{Int}
    taken_incentives::Int
    taken_incentives_full::Int
    paid_incentives::Float
    van_transports::Int
    van_transports_bikes::Int
end

function servicelevel(stats::SystemStats)
    potential_customers = stats.total_customers + sum(stats.stationempty)
    happy_customers = stats.total_customers - sum(stats.stationfull)
    return float(happy_customers)/float(potential_customers)
end

type DetailedStationStats
    station::Int
    amount::Vector{Int}
    arriving::Vector{Int}
    leaving::Vector{Int}
    full_events::Vector{Int}
    empty_events::Vector{Int}
    routed::Vector{Int}
    incentive_leave::Vector{Int}
    incentive_come::Vector{Int}
end

function createSystem(truckamount::Int)
    stationcount = 356
    slices = 72
    
    stations = dlmread("system_description/stations_status.txt", Int) # state captured during the night
    stationstatus = Array(Station,stationcount)
    for i=1:size(stations,1)
        stationstatus[i] =  Station(stations[i,1], stations[i,2], stations[i,1]+stations[i,2])
    end
    
    awork = dlmread("system_description/arrival_workday.txt", ",", Float32)
    awend = dlmread("system_description/arrival_weekend.txt", ",", Float32)
    dwork = dlmread("system_description/departure_workday.txt", ",", Float32)
    dwend = dlmread("system_description/departure_weekend.txt", ",", Float32)
    wend_history_length = 28.0 * 20.0      # minutes we have observed each slice in the history .. that is 20 minutes each day
    work_history_length = (97.0 - 28.0) * 20.0

    awork = awork / work_history_length # arrival within one minute
    awend = awend / wend_history_length
    dwork = dwork / work_history_length # arrival within one minute
    dwend = dwend / wend_history_length

    # now put everything in a 3D-array where the last dimension is the timeslice index
    departure_work = Array(Float32,stationcount,stationcount,slices)
    departure_wend = Array(Float32,stationcount,stationcount,slices)
    departure_work_sum = Array(Float32,stationcount,slices)
    departure_wend_sum = Array(Float32,stationcount,slices)
    arrival_work_sum = Array(Float32,stationcount,slices)
    arrival_wend_sum = Array(Float32,stationcount,slices)
    for i = 1:slices
        departure_work[:,:,i] = dwork[(i-1)*stationcount+1:i*stationcount,:]
        departure_wend[:,:,i] = dwend[(i-1)*stationcount+1:i*stationcount,:]
        departure_work_sum[:,i] = sum(dwork[(i-1)*stationcount+1:i*stationcount,:],2)
        departure_wend_sum[:,i] = sum(dwend[(i-1)*stationcount+1:i*stationcount,:],2)
        arrival_work_sum[:,i] = sum(awork[(i-1)*stationcount+1:i*stationcount,:],2)
        arrival_wend_sum[:,i] = sum(awend[(i-1)*stationcount+1:i*stationcount,:],2)
    end
    behavior = CustomerBehavior(departure_work,departure_wend,departure_work_sum, departure_wend_sum, arrival_work_sum,arrival_wend_sum,slices)

    tt = dlmread("system_description/average_time_from_to.txt", ",", Float32)
    sdc = dlmread("system_description/station_distance_class.txt", ",", Int)
    
    ns = dlmread("system_description/neighbour_dist.txt", ",")
    neighbourcount = int(size(ns,1)/stationcount)
    neighbours = Array(Neighbour,stationcount,neighbourcount)
    for i=1:stationcount, j=1:neighbourcount
        pos = (i-1)*neighbourcount+j
        neighbourstation = int(ns[pos,2])
        dist = ns[pos,3]
        neighbours[i,j] = Neighbour(i,neighbourstation,dist)
    end

    if truckamount == 0
        trucks = Array(Truck,0)
    else
        trucks = map(Truck, zeros(Int,truckamount))
    end
    return BikeSystem(stationcount, stationstatus, behavior, tt, sdc, neighbourcount, neighbours, 346, Array(Trip,0), truckamount, trucks)
end