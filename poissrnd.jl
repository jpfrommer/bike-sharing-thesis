# poissrnd from MATLAB

function poissrnd{T<:Real}(lambda::Array{T})

    r = zeros(Int,size(lambda))

    r[find(lambda .== Inf)] = Inf
    r[find(lambda .< 0)] = NaN

    # For large lambda, use the method of Ahrens and Dieter as
    # described in Knuth, Volume 2, 1998 edition.
    k = find((15 .<= lambda) & (lambda .< Inf))
    if ~isempty(k)
        alpha = 7/8
        lk = lambda[k]
        m = floor(alpha * lk)

        # Generate m waiting times, all at once
        x = myrandg(m)
        t = (x <= lk)

        # If we did not overshoot, then the number of additional times
        # has a Poisson distribution with a smaller mean.
        r[k[t]] = m[t] + poissrnd(lk[t]-x[t]);

        # If we did overshoot, then the times up to m-1 are uniformly
        # distributed on the interval to x, so the count of times less
        # than lambda has a binomial distribution.
        r[k[~t]] = mybinornd(m[~t]-1, lk[~t]./x[~t]);
    end

    # For small lambda, generate and count waiting times.
    j = find(lambda .< 15);
    p = zeros(numel(j),1);
    while ~isempty(j)
        p = p - log(rand(numel(j),1));
        t = (p .< lambda[j]);
        j = j[find(t)];
        p = p[find(t)];
        r[j] = r[j] + 1;
    end
 
    return r
    
end

function poissrnd(lambda::Real)
    return poissrnd([lambda])[1]
end

function myrandg(lambda::Array{Float64})
    A = Array(Float, size(lambda))
    for i = 1:numel(lambda)
        A[i] = randg(lambda[i])
    end
    return A
end

function mybinornd{T<:Real}(n::Array{Int}, pp::Array{T})
    p = float(pp)
    sz = size(n)
    rnd = zeros(sz)

    k = !(n .>= 0) | !(n .< Inf) | !(p .>= 0) | !(p .<= 1)
    rnd[k] = NaN
    
    k = (n .> 0) & (n .< Inf) & (p .>= 0) & (p .<= 1)
    if (any (k[:]))
        N = max(n[k])
        L = sum (k[:])
        tmp = rand(N, L)
        ind = repmat(1:N, 1, L)
        rnd[k] = sum ((tmp < repmat(p[k][:]', N, 1)) &
                      (ind <= repmat(n[k][:]', N, 1)), 1)
    end

    return int(rnd)
end
