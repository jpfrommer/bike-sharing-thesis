load("system.jl")
load("poissrnd.jl")
load("util.jl")
load("best_fill.jl")
load("repositioning.jl")
load("controller.jl")
load("mosek_qp_interface.jl")

_jl_tcp_interface = dlopen("interfaces/tcp_interface");
incentives_per_payout = dlmread("system_description/lin_inc_prob_direct.txt",",",Float) # linearized customer behavior
#incentives_per_payout = dlmread("system_description/lin_inc_prob.txt",",",Float) # linearized customer behavior

function batchSimulation(sys::BikeSystem, bfc::BestFillCache, t_epoch::Int, T::Int, dt::Float, priceController::Function)
    # dt = 10 # in minutes
    time_since_routing = 0
    
    stats = SystemStats(0,0,zeros(Int,sys.stationcount),zeros(Int, sys.stationcount),0,0,0.0,0,0)
    routing = Routing(Array(Int,0,0), Array(Int,0,0), Array(Int,0,0), Array(Int,0,0)) # nothing is happening. Trucks are at the depot

    totalgenerated = 0
    
    for step = 1:T
        println(step)
        date = gmtime(t_epoch)
        cslice = current_slice(date,sys.behavior.slices)
        
        # 1) recalc the routing
        if time_since_routing >= 30.0
            println(t_epoch)
            # repositioning is happening only between 8am and 8pm
            if date.hour < 7 || date.hour > 22
                routing = Routing(Array(Int,0,0), Array(Int,0,0), Array(Int,0,0), Array(Int,0,0)) # nothing is happening. Trucks are at the depot
            elseif date.hour == 7 && length(routing.stations) == 0 # starting in the morning
                routing = Routing(zeros(Int,sys.truckcount,1), zeros(Int,sys.truckcount,1), zeros(Int,sys.truckcount,1), zeros(Int,sys.truckcount,1))
                routing.stations[:,1] = sys.depot
                routing.t_epoch[:,1] = t_epoch
                routing = repositioningRouting(sys, bfc, t_epoch, 8, false, 4, routing)
            else
                # We update the routing every hour .. with the greedy algorithm we don't need a long horizon
                if date.hour == 22
                    routing = repositioningRouting(sys, bfc, t_epoch, 8, true, 4, routing) # at 7pm, return to the depot after the next hour
                else
                    routing = repositioningRouting(sys, bfc, t_epoch, 8, false, 4, routing) # normal case
                end
            end
            println(routing)
            time_since_routing = 0
        end

        # 2) Realize new customers from the Poisson lambdas
        if isbusday(date)
            new_customers = poissrnd(sys.behavior.departure_workday[:,:,cslice] .* dt)
        else
            new_customers = poissrnd(sys.behavior.departure_weekend[:,:,cslice] .* dt)
        end

        totalgenerated+=sum(new_customers)
        
        # 3) Calc controller behaviour
        control = priceController(sys, bfc, routing, incentives_per_payout, t_epoch, 10)
        
        # 5) Update world
        update_world!(sys, bfc, new_customers, control, routing, t_epoch, dt, stats)

        println(sys.trucks)
        
        stats.time += dt
        t_epoch += int(round(dt*60))
        time_since_routing += dt
    end
    println(totalgenerated)
    return stats
end

function liveSimulation(sys::BikeSystem, bfc::BestFillCache, priceController::Function, port::Int)
    
    stats = SystemStats(0,0,zeros(Int,sys.stationcount),zeros(Int, sys.stationcount),0,0,0.0,0,0)

    println("listening for a connection")

    connfd = ccall(dlsym(_jl_tcp_interface, :createServerAndListen), Int32, (Int32,), port)
    println(connfd)
    if connfd == -1
        return
    end

    println("connection established")

    last_t_epoch = 0
    routing_update = 0
    routing = false
    while true
        # 1) Receive message
        buflen = int32(1024)
        buf = Array(Uint8, buflen)
        message = ""
        while true
            mes_len = ccall(dlsym(_jl_tcp_interface, :receiveMessage), Int32, (Ptr{Uint8},Int32,Int32), buf, buflen, connfd)
            if mes_len == -1 || mes_len == 0
                break
            end
            message = strcat(message, ascii(buf[1:mes_len]))
            if mes_len < buflen
                break
            end
        end

        println(message)
        input = (split(message))
        t_epoch = int(input[1]) # in secs
        if last_t_epoch == 0
            last_t_epoch = t_epoch
        end
        dt = max(0.0,(t_epoch - last_t_epoch)/60.0) # was in seconds
        control_value = int(input[2])
        
        date = gmtime(t_epoch)
        cslice = current_slice(date,sys.behavior.slices)

        # 1) recalc the routing
        if t_epoch - routing_update >= 1800
            # repositioning is happening only between 8am and 8pm
            if date.hour < 7 || date.hour > 22
                routing = Routing(Array(Int,0,0), Array(Int,0,0), Array(Int,0,0), Array(Int,0,0)) # nothing is happening. Trucks are at the depot
            elseif date.hour == 7 # starting in the morning
                routing = Routing(Array(Int,sys.truckcount,1), Array(Int,sys.truckcount,1), Array(Int,sys.truckcount,1), Array(Int,sys.truckcount,1))
                routing.stations[:,1] = sys.depot
                routing.t_epoch[:,1] = t_epoch
                routing.delta_bikes[:,1] = 0
                routing.loaded_bikes[:,1] = 0
                routing = repositioningRouting(sys, bfc, t_epoch, 7, false, 2, routing)
            else
                # We update the routing every hour .. with the greedy algorithm we don't need a long horizon
                if date.hour == 22
                    routing = repositioningRouting(sys, bfc, t_epoch, 7, true, 2, routing) # at 7pm, return to the depot after the next hour
                else
                    routing = repositioningRouting(sys, bfc, t_epoch, 7, false, 2, routing) # normal case
                end
            end
            routing_update = t_epoch
        end

        # 2) Realize new customers from the Poisson lambdas
        if isbusday(date)
            new_customers = poissrnd(sys.behavior.departure_workday[:,:,cslice] .* dt)
        else
            new_customers = poissrnd(sys.behavior.departure_weekend[:,:,cslice] .* dt)
        end
        
        # 3) Calc controller behaviour
        control = priceController(sys, bfc, routing, incentives_per_payout, t_epoch, 10)
        
        # 5) Update world
        new_bikes, new_trucks = update_world!(sys, bfc, new_customers, control, routing, t_epoch, dt, stats)

        # 6) Send message
        # mesage format is <new bikes>\n\n<new trucks>\n\n<stations>\n\n
        message = ""
        for i=1:length(new_bikes) b = new_bikes[i]; message = strcat(message, string(b.from), ",", string(b.to), ",", string(b.time_remaining*60), "\n"); end
        message = strcat(message, "\n")

        for i=1:length(new_trucks) t = new_trucks[i]; message = strcat(message, string(t.from), ",", string(t.to), ",", string(int(t.time_remaining*60)), "\n"); end
        message = strcat(message, "\n")

        inci, incj = findn(control)
        for i=1:length(inci) message = strcat(message, string(inci[i]), ",", string(incj[i]), ",", string(control[inci[i],incj[i]]), "\n") end
        message = strcat(message, "\n")

        for i=1:sys.stationcount s = sys.stations[i]; message = strcat(message, string(s.bikes), ",", string(s.free), "\n"); end
        message = strcat(message, "\n")

        ccall(dlsym(_jl_tcp_interface, :sendMessage), Int32, (Ptr{Uint8}, Int32, Int32), message, int32(length(message)), connfd)
        
        stats.time += int(dt)
        last_t_epoch = t_epoch
    end
    return (sys, stats)
end

function update_world!(sys::BikeSystem, bfc::BestFillCache, nc::Array{Int,2}, control::Array{Float64,2}, routing::Routing, t_epoch::Int, dt::Float, stats::SystemStats)
    new_bikes = Array(Trip,0)
    new_trucks = Array(Trip,0)
    max_customer_distance_value = 20 # in euro per kilometer

    best_fill = get_best_fill(bfc,t_epoch)
    t5 = t5_from_epoch(t_epoch)
    bfs = best_fill[:,t5]

    # 3) Manual Transports (Trucks)
    truck, nths = findn(t_epoch-(dt*60) .< routing.t_epoch .<= t_epoch) # we assume transports to take long enough .. now we don't miss any
    for i = 1:length(truck)
        delta_bikes = routing.delta_bikes[truck[i],nths[i]]
        station = routing.stations[truck[i],nths[i]]
        loaded_bikes = sys.trucks[truck[i]].loaded_bikes
        if delta_bikes < 0
            delta_bikes = max(delta_bikes, -sys.stations[station].bikes, -20+loaded_bikes)
        else
            delta_bikes = min(delta_bikes, sys.stations[station].free, loaded_bikes)
        end
        sys.stations[station].bikes += delta_bikes
        sys.stations[station].free -= delta_bikes
        sys.trucks[truck[i]].loaded_bikes -= delta_bikes

        print("action ")
        print(delta_bikes)
        print(" by ")
        print(truck[i])
        print(" at ")
        println(station)
        
        stats.van_transports += 1
        stats.van_transports_bikes += abs(delta_bikes)

        if nths[i] >= size(routing.stations,2)
            continue
        end
        next = 0
        if size(routing.stations,2) >= nths[i]+1
            next = routing.stations[truck[i],nths[i]+1]
        end
        if next == 0
            continue
        end
        
        duration = sys.transition_time_class[station,next] * 5 # in 5 minute blocks
        push(new_trucks,Trip(station,next,0,duration+dt,Array(Int,0))) # this is only for visualization
    end

    # customers who want to take a bikes and customers who want to bring a bike "overlap"
    leaving_sum = sum(nc,2)
    
    # 4) Some customers arrive
    queuelength = length(sys.trips)
    for i=[queuelength:-1:1]
        if sys.trips[i].time_remaining-dt > 0.0
            sys.trips[i].time_remaining = sys.trips[i].time_remaining - dt
            continue
        end
        # customer has arrived
        to = sys.trips[i].to
        for c=1:sys.trips[i].amount
            # 4.1) Find the neighbour that has the best value (including the incentive).
            neighbours = sys.neighbours[to,:]
            value = zeros(Float64,length(neighbours))
            if int(bfs[to].plateau_start) >= sys.stations[to].bikes
                control[to,:] = 0.0 # we already don't have enough bikes.. don't put away
            end
            customer_distance_value = rand() * max_customer_distance_value
            for n = 1:sys.neighbourcount
                id = neighbours[n].neighbourid
                dist = neighbours[n].distance
                # post-processing
                if int(bfs[id].plateau_end) <= sys.stations[id].bikes
                    value[n] = -dist*customer_distance_value
                elseif int(bfs[to].plateau_start) >= sys.stations[to].bikes
                    value[n] = -dist*customer_distance_value
                else
                    value[n] = control[to,id] - dist*customer_distance_value
                end
            end
            sorted_values,sorted_neighbour_id = sortperm(value)
            sorted_values = reverse(sorted_values)
            sorted_neighbour_id = reverse(sorted_neighbour_id)
            
            history = sys.trips[i].already_visited
            # which of the best neighbours has not yet been visited? Avoid circles
            best_neighbour_station = 0
            best_neighbour_value = 0
            for n=1:sys.neighbourcount
                neighbour_station = neighbours[sorted_neighbour_id[n]].neighbourid
                if contains(history,neighbour_station)
                    continue
                end
                best_neighbour_station = neighbour_station
                best_neighbour_value = sorted_values[n]
                break
            end
            # 4.2) Customers arrives at a station with enough space
            if sys.stations[to].free+leaving_sum[to] >= 1
                if best_neighbour_value <= 0.0
                    # customer stays here as no incentive has a perceived value > 0
                    sys.stations[to].free = sys.stations[to].free - 1
                    sys.stations[to].bikes = sys.stations[to].bikes + 1
                else
                    # take incentive to one of the neighbours
                    push(history,to)
                    push(sys.trips,Trip(to,best_neighbour_station,1,sys.transition_time[to,best_neighbour_station]+dt,Array(Int,0)))
                    push(new_bikes,Trip(to,best_neighbour_station,1,sys.transition_time[to,best_neighbour_station]+dt,Array(Int,0)))
                    stats.paid_incentives += control[to,best_neighbour_station]
                    stats.taken_incentives += 1
                end

            # 4.3) Customer arrives at a station, but there is not enough space left
            else
                if best_neighbour_value <= 0.0
                    stats.stationfull[to] += 1
                end
                push(history,to)
                if best_neighbour_station == 0
                    println("we lost a bike! no neighbour station available: #", string(to))
                else
                    push(sys.trips,Trip(to,int(best_neighbour_station),1,sys.transition_time[to,best_neighbour_station]+dt,history))
                    push(new_bikes,Trip(to,int(best_neighbour_station),1,sys.transition_time[to,best_neighbour_station]+dt,history))
                    if control[to,best_neighbour_station] > 0.0
                        stats.paid_incentives += control[to,best_neighbour_station]
                        stats.taken_incentives_full += 1
                        stats.taken_incentives += 1
                    end
                end
            end
        end
        del(sys.trips,i)
    end

    # 2) add new customers to the queue
    nci, ncj = findn(nc)
    nccount = length(nci)
    for i = 1:nccount
        from = nci[i]
        to = ncj[i]
        amount = nc[from,to]
        accepted_amount = min(amount, sys.stations[from].bikes)
        rejected_amount= amount - accepted_amount
        if accepted_amount > 0
            duration = sys.transition_time[from,to] / 60
            # was in seconds
            amount = nc[from,to]
            push(sys.trips,Trip(from,to,accepted_amount,duration+dt,Array(Int,0)))
            push(new_bikes,Trip(from,to,accepted_amount,duration+dt,Array(Int,0)))
            sys.stations[from].free = sys.stations[from].free+accepted_amount
            sys.stations[from].bikes = sys.stations[from].bikes-accepted_amount
            stats.total_customers += accepted_amount
        end
        stats.stationempty[from] += rejected_amount
    end
    
    return (new_bikes, new_trucks)
end
    
