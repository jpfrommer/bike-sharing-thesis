# Routing of repositioning vehicles

require("util.jl")
require("system.jl")
require("best_fill.jl")

type StationTimeGraph
    # state vertices are implicit
    dead::Matrix{Bool} # can this vertex still reach the depot in time?
    bikes::Matrix{Float32} # amount of bikes present at the end of each state
    net_arrival::Matrix{Float32} # what would happen in this phase if bikes/space were available?

    function StationTimeGraph(dead::Matrix{Bool}, bikes::Matrix{Float32}, net_arrival::Matrix{Float32})
        return new(dead,bikes,net_arrival)
    end
    
    function StationTimeGraph(sys::BikeSystem, starttime_epoch::Int, horizon::Int, return_to_depot::Bool)
        stg = new(zeros(Bool,sys.stationcount, horizon), zeros(Float32,sys.stationcount, horizon), zeros(Float32,sys.stationcount, horizon) )

        # 1) Fill the stg with the expected bikes at each station without repositioning and save the net arrival that might be realized
        for s=1:sys.stationcount
            stg.bikes[s,1] = sys.stations[s].bikes
        end

        for t=2:horizon
            time = int(starttime_epoch+(t-1)*300) # 5 minute steps
            net_arrival = netArrival(sys,time)
            stg.net_arrival[:,t] = net_arrival
            for s=1:sys.stationcount
                new_gross_customers = stg.bikes[s,t-1]+net_arrival[s]
                stg.bikes[s,t] = max(0, min(new_gross_customers, sys.stations[s].total))
            end
        end

        # 2) Mark dead vertices from which the depot cannot be reached in time
        if return_to_depot
            stg.dead[:,horizon] = ones(Bool,sys.stationcount)
            stg.dead[sys.depot,horizon] = false
            for i = 1:12
                # every RV can reach the depot if it has more than 1h. we assume that if there is no fast enough direct connection, there is also none leading over another station
                stg.dead[:,horizon+1-i] = sys.transition_time_class[:,sys.depot] .<= i
                stg.dead[sys.depot,horizon+1-i] = false
            end
        end
        return stg
    end
end

function copy(stg::StationTimeGraph)
    return StationTimeGraph(copy(stg.dead), copy(stg.bikes), copy(stg.net_arrival))
end

type Route
    t5::Array{Int,1} # in 5 minute steps. reference point is start_t_epoch in the stg
    stations::Array{Int,1} # where is the truck at the time
    delta_bikes::Array{Int,1} # how many bikes are being picked up? (unloading is handled automatically since we generate Trips in the simulation)
    loaded_bikes::Array{Int,1}
end

function copy(route::Route)
    return Route(copy(route.t5), copy(route.stations), copy(route.delta_bikes), copy(route.loaded_bikes))
end

# Matrices: i is the truck, j is the action instance
type Routing
    t_epoch::Array{Int,2} # mapped
    stations::Array{Int,2}
    delta_bikes::Array{Int,2} # from the station point of view. -20 ~ take 20 bikes
    loaded_bikes::Array{Int,2}

    function Routing(t_epoch::Array{Int,2}, stations::Array{Int,2}, delta_bikes::Array{Int,2}, loaded_bikes::Array{Int,2})
        return new(t_epoch, stations, delta_bikes, loaded_bikes)
    end

    function Routing(route_vector::Array{Route,1}, start_t_epoch::Int)
        rv_amount = length(route_vector)
        routing_length = 0
        for rv = 1:rv_amount
            if length(route_vector[rv].stations) > routing_length
                routing_length = length(route_vector[rv].stations)
            end
        end
        
        route_matrix = Routing(zeros(Int, rv_amount, routing_length),zeros(Int, rv_amount, routing_length),zeros(Int, rv_amount, routing_length),zeros(Int, rv_amount, routing_length))
        for rv = 1:rv_amount
            thislength = length(route_vector[rv].stations)
            route_matrix.t_epoch[rv,1:thislength] = ((route_vector[rv].t5-1) * 300) + start_t_epoch
            route_matrix.stations[rv,1:thislength] = route_vector[rv].stations
            route_matrix.delta_bikes[rv,1:thislength] = route_vector[rv].delta_bikes
            route_matrix.loaded_bikes[rv,1:thislength] = route_vector[rv].loaded_bikes
        end
        return route_matrix
    end
end

# Used to determine whether a station is already blocked by a truck
type CollisionLock
    t5::Int
    rv::Int
end

function netArrival(sys::BikeSystem, time::Int)
    date = gmtime(time)
    slice = current_slice(date,sys.behavior.slices)
    if isbusday(date)
        net_arrival = sys.behavior.arrival_workday_sum[:,slice] - sys.behavior.departure_workday_sum[:,slice]
    else
        net_arrival = sys.behavior.arrival_weekend_sum[:,slice] - sys.behavior.departure_weekend_sum[:,slice]
    end
    return net_arrival
end

function updateStationTimeGraph(sys::BikeSystem, stg::StationTimeGraph, change_s::Int, change_t5::Int, delta_bike_amount::Int)
    stg.bikes[change_s,change_t5] += delta_bike_amount
    for t=change_t5+1:size(stg.bikes,2)
        new_gross_customers = stg.bikes[change_s,t-1]+stg.net_arrival[change_s,t]
        stg.bikes[change_s,t] = max(0, min(new_gross_customers, sys.stations[change_s].total))
    end
end

function insertRoutesToStationTimeGraph(sys::BikeSystem, stg::StationTimeGraph, rv_vector::Array{Route,1})
    t5s = Array(Int,0)
    stations = Array(Int,0)
    amounts = Array(Int,0)
    for rv = 1:length(rv_vector)
        t5s = [t5s; rv_vector[rv].t5]
        stations = [stations; rv_vector[rv].stations]
        amounts = [amounts; rv_vector[rv].delta_bikes]
    end

    sorted_id = sortperm(t5s)[2]
    for i = 1:length(sorted_id)
        id = sorted_id[i]
        t5 = t5s[id]
        station = stations[id]
        amount = amounts[id]
        updateStationTimeGraph(sys,stg,station,t5,amount)
    end
end

function buildInitialRouteVectorFromRouteMatrix(routing::Routing, start_t_epoch::Int)
    rv_amount = size(routing.stations,1)
    startindex = Array(Int,rv_amount)
    for rv = 1:rv_amount
        index = findfirst(routing.t_epoch[rv,:] .>= start_t_epoch)
        if index < 1
            startindex[rv] = max(1, findmax(find(routing.t_epoch[rv,:])))
        else
            startindex[rv] = index
        end
    end

    now_t5 = t5_from_epoch(start_t_epoch)
    route_vector = Array(Route,rv_amount)
    for rv = 1:rv_amount
        
        route_vector[rv] = Route([t5_from_epoch(routing.t_epoch[rv,startindex[rv]])-now_t5+1], # 1 is now. such that we can easily find the corresponding entry on the stg
                                 [routing.stations[rv,startindex[rv]]],
                                 [routing.delta_bikes[rv,startindex[rv]]],
                                 [routing.loaded_bikes[rv,startindex[rv]]])
        # if the last known place was reached before this epoch, duplicate
        if route_vector[rv].t5[1] < 1
            println("no overlap with the last period")
            route_vector[rv].t5[1] = 1
        end
    end
    
    return route_vector
end

function repositioningRouting(sys::BikeSystem, bfc::BestFillCache, starttime_epoch::Int64, horizon::Int64, return_to_depot::Bool, max_depth::Int, oldrouting::Routing)
    rv_amount = size(oldrouting.loaded_bikes,1)
    best_fill = get_best_fill(bfc,starttime_epoch)

    if rv_amount == 0
        return Routing(Array(Int,0,0), Array(Int,0,0), Array(Int,0,0), Array(Int,0,0))
    end

    stg = StationTimeGraph(sys, starttime_epoch, horizon+50, return_to_depot) # longer horizon.. when return_to_depot is false, the last tour might be longer..
    orig_stg = copy(stg)
    route_vector = buildInitialRouteVectorFromRouteMatrix(oldrouting, starttime_epoch)
    # overwrite the loaded_bikes computed during the last routing with the real amount we currently have..
    for i=1:sys.truckcount
        loaded_bikes = sys.trucks[i].loaded_bikes
        delta_bikes = route_vector[i].delta_bikes[1]
        station = route_vector[i].stations[1]
        arrival_t = route_vector[i].t5[1]
        if delta_bikes < 0
            delta_bikes = int(ceil(max(delta_bikes, -stg.bikes[station,arrival_t], -20+loaded_bikes)-0.01))
        else
            delta_bikes = int(floor(min(delta_bikes, sys.stations[station].total - stg.bikes[station,arrival_t], loaded_bikes)+0.01))
        end
        loaded_after = loaded_bikes - delta_bikes
        route_vector[i].loaded_bikes[1] = loaded_after
        route_vector[i].delta_bikes[1] = delta_bikes # because loaded_bikes is _after_ the repositioning action
    end
    global_t5_diff = t5_from_epoch(starttime_epoch) - 1
    
    # the first station is fixed
    lastindex = cell(rv_amount)
    for i=1:rv_amount
        lastindex[i] = [1]
    end

    stglock = Array(CollisionLock, sys.stationcount) # stations that are visited by a truck are blocked for all trucks until then.
    for i=1:sys.stationcount
        stglock[i] = CollisionLock(0,0)
    end

    # incorporate first arrival station into the stg
    arrival_t = Array(Int,rv_amount)
    for rv = 1:rv_amount
        arrival_t[rv] = route_vector[rv].t5[1]
    end
    for rv = sortperm(arrival_t)[2]
        s = route_vector[rv].stations[1]
        t5 = route_vector[rv].t5[1]
        # insert first arrival into the stg
        updateStationTimeGraph(sys,stg,s,t5,route_vector[rv].delta_bikes[1])
        # for the first station where a truck arrives, we mark the station dead before the arrival t5. do not go there! since we cannot reroute the other truck (that is already under way)
        for t = 1:t5-1
            stg.dead[s,t] = true
        end
    end

    while true
        println(route_vector)
        # Which rv is next?
        min_t5 = 999
        rv = 0
        for i = 1:rv_amount
            if last(route_vector[i].t5) < min_t5 && last(route_vector[i].t5) < horizon
                min_t5 = last(route_vector[i].t5)
                rv = i
            end
        end
        if rv == 0
            break
        end
        
        push(lastindex[rv],length(route_vector[rv].stations))
        # undo last action of the preceding route such that we can change the action before we actually arrive.
        updateStationTimeGraph(sys,stg,last(route_vector[rv].stations),last(route_vector[rv].t5),-last(route_vector[rv].delta_bikes))
        # save a clean copy of the stg for the linear optimization.
        clean_stg = copy(stg)

        routingStep(sys, stg, clean_stg, best_fill, route_vector[rv], 1, max_depth, last(lastindex[rv]), global_t5_diff)

        # delete the last entry of the new route
        route_vector[rv].t5 = route_vector[rv].t5[1:end-1]
        route_vector[rv].stations = route_vector[rv].stations[1:end-1]
        route_vector[rv].delta_bikes = route_vector[rv].delta_bikes[1:end-1]
        route_vector[rv].loaded_bikes = route_vector[rv].loaded_bikes[1:end-1]

        # detect collisions
        collision = false
        first_collision_t5 = 99999
        colliding_station = 0
        colliding_rv = 0
        for i = last(lastindex[rv]):length(route_vector[rv].stations)
            station = route_vector[rv].stations[i]
            t5 = route_vector[rv].t5[i]
            if stglock[station].t5 > t5 && t5 < first_collision_t5 && station != sys.depot
                collision = true
                colliding_rv = stglock[station].rv
            end
        end

        if !collision
            # adapt the stg (simple version)
            for i = last(lastindex[rv]):length(route_vector[rv].stations)
                thisstation = route_vector[rv].stations[i]
                this_t = route_vector[rv].t5[i]
                delta_bikes = route_vector[rv].delta_bikes[i]
                updateStationTimeGraph(sys,clean_stg,thisstation,this_t,delta_bikes)
                stglock[thisstation] = CollisionLock(this_t,rv)
            end
            stg = clean_stg
        else
            println("=== collision ===")

            # undo the current rv but for the first step
            endindex = last(lastindex[rv])+1
            route_vector[rv].t5 = route_vector[rv].t5[1:endindex]
            route_vector[rv].stations = route_vector[rv].stations[1:endindex]
            route_vector[rv].delta_bikes = route_vector[rv].delta_bikes[1:endindex]
            route_vector[rv].loaded_bikes = route_vector[rv].loaded_bikes[1:endindex]

            # undo the colliding rv completely. this step will be recomputed.
            if length(lastindex[colliding_rv]) == 1
                endindex = lastindex[colliding_rv][1]
            else
                endindex = pop(lastindex[colliding_rv]) # this also removes that entry..
            end
            route_vector[colliding_rv].t5 = route_vector[colliding_rv].t5[1:endindex]
            route_vector[colliding_rv].stations = route_vector[colliding_rv].stations[1:endindex]
            route_vector[colliding_rv].delta_bikes = route_vector[colliding_rv].delta_bikes[1:endindex]
            route_vector[colliding_rv].loaded_bikes = route_vector[colliding_rv].loaded_bikes[1:endindex]
            
            # reset the stg and load the current rv_vector
            stg = copy(orig_stg)
            insertRoutesToStationTimeGraph(sys, stg, route_vector)

            # reset collision detection
            for i=1:sys.stationcount
                stglock[i] = CollisionLock(0,0)
            end
            for rv = 1:rv_amount
                for i = 1:length(route_vector[rv].stations)
                    station = route_vector[rv].stations[i]
                    t5 = route_vector[rv].t5[i]
                    if t5 > stglock[station].t5
                        stglock[i] = CollisionLock(t5,rv)
                    end
                end
            end
        end
    end
    
    return Routing(route_vector, starttime_epoch)
end

# startindex is the index of the station where the amount taken or left may still be changed. but the station is fix.
function routingStep(sys::BikeSystem, stg::StationTimeGraph, clean_stg::StationTimeGraph, bf::Array{BestFillStatus,2}, route::Route, depth::Int, max_depth::Int, startindex::Int, global_t5_diff::Int)

    if depth >= max_depth
        enhanceRoutingWithPlateau(sys, clean_stg, bf, route, startindex-1, global_t5_diff)
        return routingValue(stg, bf, route, global_t5_diff, startindex) / (last(route.t5) - route.t5[startindex])
    end

    current_t5 = last(route.t5)
    current_s = last(route.stations)
    loaded_bikes = last(route.loaded_bikes)
    
    actionvalue = Array(Float,sys.stationcount)
    besttakebufferstation = 0
    besttakebuffervalue = 0.0
    bestbringbufferstation = 0
    bestbringbuffervalue = 0.0
    
    for s=1:sys.stationcount
        distance = sys.transition_time_class[current_s,s]
        arrival_t = distance+current_t5
        current_bikes = stg.bikes[s,arrival_t]
        best_action = bestAction(bf, s, min(288,global_t5_diff+arrival_t), current_bikes)
        # what are the most relevant stations?
        if s != sys.depot && (stg.dead[s,arrival_t] || current_s == s)
            actionvalue[s] = -9999
        else
            bfs = bf[s,min(288,global_t5_diff+arrival_t)]
            if best_action > loaded_bikes
                best_action = loaded_bikes # this is how many we can leave
            elseif -best_action + loaded_bikes > 20
                best_action = loaded_bikes - 20 # this is how many we can take away
            end
            actionvalue[s] = abs(best_action) / distance
            bringbuffervalue = min(15, bfs.plateau_end - current_bikes) / distance
            takebuffervalue = min(15, current_bikes - bfs.plateau_start) / distance

            if bringbuffervalue > bestbringbuffervalue
                bestbringbuffervalue = bringbuffervalue
                bestbringbufferstation = s
            end
            
            if takebuffervalue > besttakebuffervalue
                besttakebuffervalue = takebuffervalue
                besttakebufferstation = s
            end
        end
    end

    relevant_stations = [sortperm(actionvalue)[2][end-5:end]]
    if !contains(relevant_stations, bestbringbufferstation) && bestbringbufferstation != 0
        relevant_stations = [relevant_stations; bestbringbufferstation]
    end
    if !contains(relevant_stations, besttakebufferstation) && besttakebufferstation != 0
        relevant_stations = [relevant_stations; besttakebufferstation]
    end
    
    optimal_route = route
    optimal_route_value = -999
    # what shall I do at the current station?
    for s = relevant_stations
        # 1) When do we arrive
        transition_time = sys.transition_time_class[current_s,s]
        arrival_t = current_t5 + transition_time
        
        if stg.dead[s,arrival_t]
            continue
        end
        
        # 2) What is the optimal choice at the station? How many bikes will be loaded afterwards?
        # At this point, we do not step into the plateau..
        here_best_action = bestAction(bf,s,min(288,global_t5_diff+arrival_t),stg.bikes[s,arrival_t])

        if here_best_action > loaded_bikes
            best_action = loaded_bikes # this is how many we can leave
        elseif -here_best_action + loaded_bikes > 20
            best_action = loaded_bikes - 20 # this is how many we can take away
        else
            best_action = here_best_action
        end

        new_loaded_bikes = loaded_bikes - best_action

        thisroute = copy(route)
        thisroute.t5 = [thisroute.t5; arrival_t]
        thisroute.stations = [thisroute.stations; s]
        thisroute.delta_bikes = [thisroute.delta_bikes; best_action]
        thisroute.loaded_bikes = [thisroute.loaded_bikes; new_loaded_bikes]

        # 3) Change the StationTimeGraph
        updateStationTimeGraph(sys,stg,s,arrival_t, best_action)
        
        # 4) Go into the recursion step
        # what is the optimal action after that?
        route_value = routingStep(sys, stg, clean_stg, bf, thisroute, depth+1, max_depth, startindex, global_t5_diff)
        if route_value > optimal_route_value
            optimal_route = thisroute
            optimal_route_value = route_value
        end
        
        # 5) Unwind the StationTimeGraph
        updateStationTimeGraph(sys,stg,s,arrival_t,-best_action)
    end

    # 6) write the optimal route into the route that is being returned over the input reference
    route.t5 = optimal_route.t5
    route.stations = optimal_route.stations
    route.delta_bikes = optimal_route.delta_bikes
    route.loaded_bikes = optimal_route.loaded_bikes
    
    # 7) Return
    return optimal_route_value
end

# Can we pickup / leave more bikes than only till the start of the plateau?
# startindex is the index in the route after which changes are allowed
function enhanceRoutingWithPlateau(sys::BikeSystem, stg::StationTimeGraph, bf::Array{BestFillStatus,2}, route::Route, startindex::Int, global_t5_diff::Int)
    # optimization vector contains
    # - delta_bikes at each station
    # - diff from start plateau
    # - diff from end plateau
    # - abs diff from start
    # - abs diff from end
    steps = length(route.stations) - startindex
    if steps <= 1
        return route
    end

    if startindex < 1
        start_bikes_loaded = route.loaded_bikes[1] + route.delta_bikes[1]
    else
        start_bikes_loaded = route.loaded_bikes[startindex] # how many bikes were loaded _before_ the enhance step?
    end
    
    A = PutOnlyMatrix(600)
    start_loaded = Array(Float64,steps) # this is only a short part.. the rest is defined directly in the c interface
    beq = Array(Float64,2*steps)

    for i = 1:steps
        station = route.stations[startindex+i]
        t5 = route.t5[startindex+i]
        bfs = bf[station,min(288, global_t5_diff+t5)]
        current_bikes = stg.bikes[station,t5]
        
        # 1) bikes_loaded always 0 <= x <= 20
        # -start_loaded <= -sum delta bikes <= 20-start_loaded
        for j = 1:i
            put!(A,i,j,-1.0)
        end
        
        # 2) Set difference from plateau start
        put!(A,steps+i,steps+i,1.0) # the diff from plateau start
        put!(A,steps+i,i,1.0) # delta bikes
        plateau_start = bfs.plateau_start
        beq[i] = plateau_start - current_bikes

        # 3) Set difference from plateau end
        put!(A,2*steps+i,2*steps+i,1.0)
        put!(A,2*steps+i,i,-1.0)
        plateau_end = bfs.plateau_end
        beq[steps+i] = -plateau_end + current_bikes

        # 4) Abs from difference from the plateau start
        # -abs - orig <= 0
        put!(A,steps*3+i,steps*3+i,-1.0)
        put!(A,steps*3+i,steps+i,-1.0)
        # orig -abs <= 0
        put!(A,steps*4+i,steps*3+i,-1.0)
        put!(A,steps*4+i,steps+i,1.0)

        # 5) Abs from difference from the plateau end
        # -abs - orig <= 0
        put!(A,steps*5+i,steps*4+i,-1.0)
        put!(A,steps*5+i,2*steps+i,-1.0)
        # orig -abs <= 0
        put!(A,steps*6+i,steps*4+i,-1.0)
        put!(A,steps*6+i,2*steps+i,1.0)
    end

    difference_penalty = ([steps-1:-1:0]*0.001) + 1.0
    c = [zeros(Float64,steps); repmat(difference_penalty, 4,1)]
    result = Array(Float64, 5*steps)
    
    mosek_refinement(int32(steps), float64(start_bikes_loaded), A.count, A.i, A.j, A.val, beq, vec(c), result)
    for i=1:steps
        route.delta_bikes[startindex+i] = int(round(result[i]))
    end

    route.loaded_bikes[startindex+1] = start_bikes_loaded - route.delta_bikes[startindex+1]
    for i=2:steps
        route.loaded_bikes[startindex+i] = route.loaded_bikes[startindex+i-1] - route.delta_bikes[startindex+i]
    end
end

function routingValue(stg::StationTimeGraph, bf::Array{BestFillStatus}, route::Route, global_t5_diff::Int, startindex::Int)
    value = 0
    for step = startindex:length(route.stations)
        t5 = route.t5[step]
        station = route.stations[step]
        amount = stg.bikes[station,t5]
        orig_amount = amount - route.delta_bikes[step]
        value += changedFillUtility(bf[station,min(288,global_t5_diff+t5)], orig_amount, amount)
    end
    return value
end