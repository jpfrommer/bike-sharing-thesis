type CustomerBehavior # the expected amount of customers to arrive/leave within one minute in the respective timeslice
    departure_workday::Array{Float32,3} # i: from which station, j: to which station, k: in which timeslice
    departure_weekend::Array{Float32,3}
    departure_workday_sum::Array{Float32,2} # sum values.. all arrivals and all departures
    departure_weekend_sum::Array{Float32,2}
    arrival_workday_sum::Array{Float32,2}
    arrival_weekend_sum::Array{Float32,2}
    slices::Int # in how many slices do we partition each day?
end

type Neighbour
    stationid::Int
    neighbourid::Int
    distance::Float
end

type Station
    bikes::Int
    free::Int
    total::Int
end

type Trip
    from::Int
    to::Int
    amount::Int
    time_remaining::Float
    already_visited::Array{Int,1} # Customers don't return to stations where they took an incentive/the station was full. This is the Tabu list
end

type BikeSystem
    stationcount::Int
    stations::Vector{Station}
    behavior::CustomerBehavior # when do customers arrive?
    transition_time::Matrix{Float32} # how long does it take on average to get from station a to b
    transition_time_class::Matrix{Int} # transition times clustered for rv routing
    neighbourcount::Int # for every station, we have the N nearest neighbours
    neighbours::Matrix{Neighbour} # i:stations, j: neighbours
    depot::Int # depot for routing vehicles
    trips::Vector{Trip} # queue for bikes that are currently under way
end

type SystemStats
    time::Int
    total_customers::Int
    stationfull::Int
    stationempty::Int
    taken_incentives::Int
    taken_incentives_full::Int
    paid_incentives::Float
    van_transports::Int
    van_transports_bikes::Int
end

function createSystem()
    stationcount = 356
    slices = 72
    
    stations = dlmread("system_description/stations_status.txt", Int) # state captured during the night
    stationstatus = Array(Station,stationcount)
    for i=1:size(stations,1)
        stationstatus[i] =  Station(stations[i,1], stations[i,2], sum(stations[i,:]))
    end
    
    awork = dlmread("system_description/arrival_workday.txt", ",", Float32)
    awend = dlmread("system_description/arrival_weekend.txt", ",", Float32)
    dwork = dlmread("system_description/departure_workday.txt", ",", Float32)
    dwend = dlmread("system_description/departure_weekend.txt", ",", Float32)
    wend_history_length = 28.0 * (24.0/slices) * 20.0      # minutes we have observed each slice in the history .. that is 20 minutes each day
    work_history_length = (97.0-28.0) * (24.0/slices) * 20.0

    awork = awork / work_history_length # arrival within one minute
    awend = awend / wend_history_length
    dwork = dwork / work_history_length # arrival within one minute
    dwend = dwend / wend_history_length

    # now put everything in a 3D-array where the last dimension is the timeslice index
    departure_work = Array(Float32,stationcount,stationcount,slices)
    departure_wend = Array(Float32,stationcount,stationcount,slices)
    departure_work_sum = Array(Float32,stationcount,slices)
    departure_wend_sum = Array(Float32,stationcount,slices)
    arrival_work_sum = Array(Float32,stationcount,slices)
    arrival_wend_sum = Array(Float32,stationcount,slices)
    for i = 1:slices
        departure_work[:,:,i] = dwork[(i-1)*stationcount+1:i*stationcount,:]
        departure_wend[:,:,i] = dwend[(i-1)*stationcount+1:i*stationcount,:]
        departure_work_sum[:,i] = sum(dwork[(i-1)*stationcount+1:i*stationcount,:],2)
        departure_wend_sum[:,i] = sum(dwend[(i-1)*stationcount+1:i*stationcount,:],2)
        arrival_work_sum[:,i] = sum(awork[(i-1)*stationcount+1:i*stationcount,:],2)
        arrival_wend_sum[:,i] = sum(awend[(i-1)*stationcount+1:i*stationcount,:],2)
    end
    behavior = CustomerBehavior(departure_work,departure_wend,departure_work_sum, departure_wend_sum, arrival_work_sum,arrival_wend_sum,slices)

    tt = dlmread("system_description/average_time_from_to.txt", ",", Float32)
    # In the vehicle routing, trips must have one of 12 possible durations from 5 to 60 minutes.
    transition_time_class = zeros(Int,stationcount,stationcount)
    transition_time_class = tt .< 7.5*60
    transition_time_class = transition_time_class + ((tt .>= 7.5*60) & (tt .< 12.5*60)) * 2
    transition_time_class = transition_time_class + ((tt .>= 12.5*60) & (tt .< 17.5*60)) * 3
    transition_time_class = transition_time_class + ((tt .>= 17.5*60) & (tt .< 22.5*60)) * 4
    transition_time_class = transition_time_class + ((tt .>= 22.5*60) & (tt .< 27.5*60)) * 5
    transition_time_class = transition_time_class + ((tt .>= 27.5*60) & (tt .< 32.5*60)) * 6
    transition_time_class = transition_time_class + ((tt .>= 32.5*60) & (tt .< 37.5*60)) * 7
    transition_time_class = transition_time_class + ((tt .>= 37.5*60) & (tt .< 42.5*60)) * 8
    transition_time_class = transition_time_class + ((tt .>= 42.5*60) & (tt .< 47.5*60)) * 9
    transition_time_class = transition_time_class + ((tt .>= 47.5*60) & (tt .< 52.5*60)) * 10
    transition_time_class = transition_time_class + ((tt .>= 52.5*60) & (tt .< 57.5*60)) * 11
    transition_time_class = transition_time_class + (tt .>= 57.5*60) * 12
    transition_time_class = transition_time_class + (tt .== 0.0) * 99999999 # dead stations are faaaar away

    transition_time_class += 1 # We add 5 constant minutes to load/unload the bikes
    
    ns = dlmread("system_description/neighbour_dist.txt", ",")
    neighbourcount = int(size(ns,1)/stationcount)
    neighbours = Array(Neighbour,stationcount,neighbourcount)
    for i=1:stationcount, j=1:neighbourcount
        pos = (i-1)*neighbourcount+j
        neighbourstation = int(ns[pos,2])
        dist = ns[pos,3]
        neighbours[i,j] = Neighbour(i,neighbourstation,dist)
    end

    return BikeSystem(stationcount,stationstatus,behavior,tt, transition_time_class,neighbourcount,neighbours,346,Array(Trip,0))
end