from pymongo.connection import Connection
import numpy

connection = Connection("localhost")
db = connection['bikedata']

stationcount = 356
slicecount = 72 # 24*3

arrival_workday = numpy.zeros((stationcount*slicecount,stationcount),dtype='float32')
arrival_weekend = numpy.zeros((stationcount*slicecount,stationcount),dtype='float32')
departure_workday = numpy.zeros((stationcount*slicecount,stationcount),dtype='float32')
departure_weekend = numpy.zeros((stationcount*slicecount,stationcount),dtype='float32')

departure_cursor = db['mr_departure_per_timelslice'].find()
for departure in departure_cursor:
    if departure['_id']['weekend'] == True:
        departure_weekend[departure['_id']['from']+departure['_id']['timeslice']*stationcount-1,departure['_id']['to']-1] = departure['value']
    else:
        departure_workday[departure['_id']['from']+departure['_id']['timeslice']*stationcount-1,departure['_id']['to']-1] = departure['value']

arrival_cursor = db['mr_arrival_per_timelslice'].find()
for arrival in arrival_cursor:
    if arrival['_id']['weekend'] == True:
        arrival_weekend[arrival['_id']['to']+arrival['_id']['timeslice']*stationcount-1,arrival['_id']['from']-1] = arrival['value']
    else:
        arrival_workday[arrival['_id']['to']+arrival['_id']['timeslice']*stationcount-1,arrival['_id']['from']-1] = arrival['value']

numpy.savetxt("arrival_workday.txt", arrival_workday,delimiter=",")
numpy.savetxt("arrival_weekend.txt", arrival_weekend,delimiter=",")
numpy.savetxt("departure_workday.txt", departure_workday,delimiter=",")
numpy.savetxt("departure_weekend.txt", departure_weekend,delimiter=",")

