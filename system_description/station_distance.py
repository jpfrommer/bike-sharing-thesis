
from pymongo.connection import Connection
import numpy
from math import radians, cos, sin, asin, sqrt, ceil

connection = Connection("localhost")
db = connection['bikedata']

stationcount = 356
slicecount = 72 # 24*3

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    km = 6367 * c
    return km 

lon = numpy.zeros((stationcount),dtype='float32')
lat = numpy.zeros((stationcount),dtype='float32')
distance = numpy.zeros((stationcount,stationcount),dtype='float32')
distance_class = numpy.zeros((stationcount,stationcount),dtype='int32')

station_cursor = db['stations'].find()
for station in station_cursor:
    if station['_id'] > stationcount:
        continue
    lon[station['_id']-1] = station['pos']['lon']
    lat[station['_id']-1] = station['pos']['lat']
    
for i in range(stationcount):
    for j in range(stationcount):
        distance[i,j] = haversine(lon[i],lat[i],lon[j],lat[j])

for i in range(stationcount):
    for j in range(stationcount):
        distance_class[i,j] = min(int(ceil(distance[i,j] / 1.25) + 1),12)

print distance_class

numpy.savetxt("station_distance_class.txt", distance_class, delimiter=",")
