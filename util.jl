# Date Handling Routines

# struct tm from <time.h>
#
# Member	Meaning	Range
# ---------------------
# sec	seconds after the minute	0-61*
# min	minutes after the hour	0-59
# hour	hours since midnight	0-23
# mday	day of the month	1-31
# mon	months since January	0-11
# year	years since 1900	
# wday	days since Sunday	0-6
# yday	days since January 1	0-365
# isdst	Daylight Saving Time flag	
#
# The Daylight Saving Time flag (tm_isdst) is greater than zero if
# Daylight Saving Time is in effect, zero if Daylight Saving Time is
# not in effect, and less than zero if the information is not
# available.
#
# * sec is generally 0-59. Extra range to accommodate for leap seconds
# * in certain systems.

require("best_fill.jl")

type Date
    sec::Int
    min::Int
    hour::Int
    mday::Int
    mon::Int
    year::Int
    wday::Int
    yday::Int
    isdst::Int
end

_jl_time_wrapper = dlopen("interfaces/gmtime_unwrap")

function gmtime(t::Int)
    t = int32(t)
    tm = ccall(:gmtime, Ptr{Void}, (Ptr{Int32},), &t)
    if tm == C_NULL
        error("getenv: undefined variable: ", tm)
    end
    # do this with a macro
    sec = ccall(dlsym(_jl_time_wrapper, "_jl_time__tm_get_sec"), Int32, (Ptr{Void},), tm)
    min = ccall(dlsym(_jl_time_wrapper, "_jl_time__tm_get_min"), Int32, (Ptr{Void},), tm)
    hour = ccall(dlsym(_jl_time_wrapper, "_jl_time__tm_get_hour"), Int32, (Ptr{Void},), tm)
    mday = ccall(dlsym(_jl_time_wrapper, "_jl_time__tm_get_mday"), Int32, (Ptr{Void},), tm)
    mon = ccall(dlsym(_jl_time_wrapper, "_jl_time__tm_get_mon"), Int32, (Ptr{Void},), tm)
    year = ccall(dlsym(_jl_time_wrapper, "_jl_time__tm_get_year"), Int32, (Ptr{Void},), tm)
    wday = ccall(dlsym(_jl_time_wrapper, "_jl_time__tm_get_wday"), Int32, (Ptr{Void},), tm)
    yday = ccall(dlsym(_jl_time_wrapper, "_jl_time__tm_get_yday"), Int32, (Ptr{Void},), tm)
    isdst = ccall(dlsym(_jl_time_wrapper, "_jl_time__tm_get_isdst"), Int32, (Ptr{Void},), tm)
    return Date(int(sec), int(min), int(hour), int(mday), int(mon), int(year), int(wday), int(yday), int(isdst))
end

function isbusday(date::Date)
    return date.wday != 0 && date.wday != 6
end

function t5_from_epoch(epoch::Int)
    d = gmtime(epoch)
    return int(d.hour*12 + floor(d.min/5)+1)
end

function get_best_fill(bfc::BestFillCache, epoch::Int)
    d = gmtime(epoch)
    if d.wday == 0
        return bfc.sunday
    elseif d.wday == 5
        return bfc.friday
    elseif d.wday == 6
        return bfc.saturday
    end
    return bfc.workday
end

function get_best_fill_2h(bfc::BestFillCache, epoch::Int)
    d = gmtime(epoch)
    if d.wday == 0
        return bfc.weekend_2h
    elseif d.wday == 5
        return bfc.workday_2h
    elseif d.wday == 6
        return bfc.weekend_2h
    end
    return bfc.workday_2h
end

function current_slice(ctime::Date,slicecount::Int)
    return int(floor((ctime.hour*60+ctime.min)/(24*60/slicecount)))+1
end

function findmax{T}(x::Vector{T})
    maxel = 1
    for i=2:length(x)
        if x[i] > x[i-1]
            maxel = i
        end
    end
    return maxel
end