/* 
Mosek C-Library Interface for the Bike Sharing Scheme Simulation written in Julia Language by Julius Pfrommer (2012)
   
Compile with
gcc -shared -Wall mosek_qp_interface.c -I/home/julius/mosek/6/tools/platform/linux64x86/h -L/home/julius/mosek/6/tools/platform/linux64x86/bin/ -lmosek64 -pthread -O3 -fPIC -DNDEBUG -o mosek_qp_interface.so

Or with debugging on as
gcc -shared -Wall mosek_qp_interface.c -I/home/julius/mosek/6/tools/platform/linux64x86/h -L/home/julius/mosek/6/tools/platform/linux64x86/bin/ -lmosek64 -pthread -O3 -fPIC -o mosek_qp_interface.so               

Or on a Mac
Normal:
gcc -Wall mosek_qp_interface.c -I/Users/julius/Documents/IT/Tools/mosek/6/tools/platform/osx64x86/h -L/Users/julius/Documents/IT/Tools/mosek/6/tools/platform/osx64x86/bin/ -lmosek64 -pthread -O3 -dynamiclib -DNDEBUG -o mosek_qp_interface.dylib
Debug:
gcc -Wall mosek_qp_interface.c -I/Users/julius/Documents/IT/Tools/mosek/6/tools/platform/osx64x86/h -L/Users/julius/Documents/IT/Tools/mosek/6/tools/platform/osx64x86/bin/ -lmosek64 -pthread -O3 -dynamiclib -o mosek_qp_interface.dylib
*/

#include <stdio.h>
#include "mosek.h" /* Include the MOSEK definition file. */

// Only for debugging
static void MSKAPI printstr(void *handle, char str[]) {printf("%s",str);} /* printstr */


// All arrays are generated on the julia side. even the vararray with the optimal results.
// All equality consraints are stored at the beginning
// ltconstraints = lesser than constraints
extern int solve_pricing_qp(int vsize, int vsize_state, int nnzA, int Ai[], int Aj[], double Aval[], double beq[], double max_payout, double Qdiag[], double result[])
{
  
  MSKrescodee  r;
  // We assume there is no linear cost term (set to 0 in code)
  // double       c[]    = {0.0,-1.0,0.0};
  
  MSKidxt      qsubi[vsize];
  MSKidxt      qsubj[vsize];
  double       qval[vsize];
  
  MSKidxt      j,i;
  double       xx[vsize];
  MSKenv_t     env;
  MSKtask_t    task;

  /* Create the mosek environment. */
  r = MSK_makeenv(&env,NULL,NULL,NULL,NULL);

  #ifndef NDEBUG
  /* Check whether the return code is ok. */
  if ( r==MSK_RES_OK )
  {
    /* Directs the log stream to the 'printstr' function. */
    MSK_linkfunctoenvstream(env,MSK_STREAM_LOG,NULL,printstr);
  }
  #endif

  /* Initialize the environment. */   
  r = MSK_initenv(env);
  if ( r==MSK_RES_OK )
  {   
  /* Create the optimization task. */
    r = MSK_maketask(env,6*vsize_state,vsize,&task);

    if ( r==MSK_RES_OK ) {
      r = MSK_linkfunctotaskstream(task,MSK_STREAM_LOG,NULL,printstr);
      
    /* Give MOSEK an estimate of the size of the input data. 
     This is done to increase the speed of inputting data. 
     However, it is optional. */
    if (r == MSK_RES_OK)
      r = MSK_putmaxnumvar(task,vsize);
  
    if (r == MSK_RES_OK)
      r = MSK_putmaxnumcon(task,2*vsize_state);
    
    if (r == MSK_RES_OK)
      r = MSK_putmaxnumanz(task,nnzA);

    /* Append 'neqconstraints' empty constraints.
     The constraints will initially have no bounds. */
    if ( r == MSK_RES_OK )
      r = MSK_append(task,MSK_ACC_CON,2*vsize_state);

    /* Append 'nvar' variables.
     The variables will initially be fixed at zero (x=0). */
    if ( r == MSK_RES_OK )
      r = MSK_append(task,MSK_ACC_VAR,vsize);

    /* /\* Set the bounds on variable j. */
    /*      blx[j] <= x_j <= bux[j] *\/ */
    for(j=0; j<vsize_state && r==MSK_RES_OK; ++j)
      r = MSK_putbound(task,
                       MSK_ACC_VAR,    /* Put bounds on variables.*/
                       j,              /* Index of variable.*/
                       MSK_BK_FR,      /* Bound key.*/
                       -MSK_INFINITY,  /* Numerical value of lower bound.*/
                       +MSK_INFINITY); /* Numerical value of upper bound.*/
      
    for(j=vsize_state; j<vsize_state*2 && r==MSK_RES_OK; ++j)
      r = MSK_putbound(task,
                       MSK_ACC_VAR,  /* Put bounds on variables.*/
                       j,            /* Index of variable.*/
                       MSK_BK_RA,    /* Bound key.*/
                       (double) 0.0, /* Numerical value of lower bound.*/
                       max_payout);  /* Numerical value of upper bound.*/
    
    // Fill constraint Matrix A
    for(i=0; i<nnzA && r == MSK_RES_OK; ++i)
      r = MSK_putaij(task,
                     Ai[i]-1,
                     Aj[i]-1,
                     Aval[i]);

    /* Set the bounds on constraints.
       for i=1, ...,neqconstraints : blc[i] <= constraint i <= buc[i] */
    for(i=0; i<vsize_state && r==MSK_RES_OK; ++i)
        r = MSK_putbound(task,
                         MSK_ACC_CON, /* Put bounds on constraints.*/
                         i,           /* Index of constraint.*/
                         MSK_BK_FX,   /* Bound key.*/
                         beq[i],      /* Numerical value of lower bound.*/
                         beq[i]);     /* Numerical value of upper bound.*/

    for(i=vsize_state; i<vsize_state*2 && r==MSK_RES_OK; ++i)
        r = MSK_putbound(task,
                         MSK_ACC_CON, /* Put bounds on constraints.*/
                         i,           /* Index of constraint.*/
                         MSK_BK_RA,   /* Bound key.*/
                         (double) 0.0,      /* Numerical value of lower bound.*/
                         (double) 1.0);     /* Numerical value of upper bound.*/

    if ( r==MSK_RES_OK ) {
      // The lower triangular part of the Q
      // matrix in the objective is specified.
      for(j=0; j<vsize; ++j) {
        qsubi[j] = j;
        qsubj[j] = j;
        qval[j] = Qdiag[j];
      }
        
      /* Input the Q for the objective. */
      r = MSK_putqobj(task,vsize,qsubi,qsubj,qval);
    }

    if ( r==MSK_RES_OK )
      r = MSK_putobjsense(task, MSK_OBJECTIVE_SENSE_MINIMIZE);      

    if ( r==MSK_RES_OK ) {
      MSKrescodee trmcode;
      /* Run optimizer */
      r = MSK_optimizetrm(task,&trmcode);

        /* Print a summary containing information
           about the solution for debugging purposes*/
#ifndef NDEBUG
        MSK_solutionsummary (task,MSK_STREAM_LOG);
  #endif
        
        if ( r==MSK_RES_OK )
        {
          MSKsolstae solsta;
          
          MSK_getsolutionstatus (task,
                                 MSK_SOL_ITR,
                                 NULL,
                                 &solsta);
          
          switch(solsta)
          {
            case MSK_SOL_STA_OPTIMAL:   
            case MSK_SOL_STA_NEAR_OPTIMAL:
              MSK_getsolutionslice(task,
                                   MSK_SOL_ITR,    /* Request the interior solution. */
                                   MSK_SOL_ITEM_XX,/* Which part of solution.        */
                                   0,              /* Index of first variable.       */
                                   vsize,           /* Index of last variable+1.      */
                                   xx);
              for(j=0; j<vsize; ++j) {
                result[j] = xx[j];
              }
              
              break;
            case MSK_SOL_STA_DUAL_INFEAS_CER:
            case MSK_SOL_STA_PRIM_INFEAS_CER:
            case MSK_SOL_STA_NEAR_DUAL_INFEAS_CER:
            case MSK_SOL_STA_NEAR_PRIM_INFEAS_CER:  
              printf("Primal or dual infeasibility certificate found.\n");
              break;
              
            case MSK_SOL_STA_UNKNOWN:
              printf("The status of the solution could not be determined.\n");
              break;
            default:
              printf("Other solution status.");
              break;
          }
        }
        else
        {
          printf("Error while optimizing.\n");
        }
      }
    
      if (r != MSK_RES_OK)
      {
        /* In case of an error print error code and description. */      
        char symname[MSK_MAX_STR_LEN];
        char desc[MSK_MAX_STR_LEN];
        
        printf("An error occurred while optimizing.\n");     
        MSK_getcodedesc (r,
                         symname,
                         desc);
        printf("Error %s - '%s'\n",symname,desc);
      }
    }
 
    MSK_deletetask(&task);
  }
  MSK_deleteenv(&env);

  return ( r );
}

extern int solve_pricing_qp_station_size(int vsize, int vsize_state, int ncount, double max_payout, int constraint_count, int nnzA, int Ai[], int Aj[], double Aval[], double b[], int optim_dir[], double c[], double Qdiag[], double result[])
{
  
  MSKrescodee  r;
  
  MSKidxt      qsubi[vsize];
  MSKidxt      qsubj[vsize];
  double       qval[vsize];
  
  MSKidxt      j,i;
  double       xx[vsize];
  MSKenv_t     env;
  MSKtask_t    task;

  /* Create the mosek environment. */
  r = MSK_makeenv(&env,NULL,NULL,NULL,NULL);

  #ifndef NDEBUG
  /* Check whether the return code is ok. */
  if ( r==MSK_RES_OK )
  {
    /* Directs the log stream to the 'printstr' function. */
    MSK_linkfunctoenvstream(env,MSK_STREAM_LOG,NULL,printstr);
  }
  #endif

  /* Initialize the environment. */   
  r = MSK_initenv(env);
  if ( r==MSK_RES_OK )
  {   
  /* Create the optimization task. */
    r = MSK_maketask(env,constraint_count,vsize,&task);

    if ( r==MSK_RES_OK ) {
      r = MSK_linkfunctotaskstream(task,MSK_STREAM_LOG,NULL,printstr);
      
    /* Give MOSEK an estimate of the size of the input data. 
     This is done to increase the speed of inputting data. 
     However, it is optional. */
    if (r == MSK_RES_OK)
      r = MSK_putmaxnumvar(task,vsize);
  
    if (r == MSK_RES_OK)
      r = MSK_putmaxnumcon(task,constraint_count);
    
    if (r == MSK_RES_OK)
      r = MSK_putmaxnumanz(task,nnzA);

    /* Append 'neqconstraints' empty constraints.
     The constraints will initially have no bounds. */
    if ( r == MSK_RES_OK )
      r = MSK_append(task,MSK_ACC_CON,constraint_count);

    /* Append 'nvar' variables.
     The variables will initially be fixed at zero (x=0). */
    if ( r == MSK_RES_OK )
      r = MSK_append(task,MSK_ACC_VAR,vsize);

    for(j=0; j<vsize_state && r==MSK_RES_OK; ++j)
      r = MSK_putbound(task,
                       MSK_ACC_VAR,    /* Put bounds on variables.*/
                       j,              /* Index of variable.*/
                       MSK_BK_FR,      /* Bound key.*/
                       -MSK_INFINITY,  /* Numerical value of lower bound.*/
                       +MSK_INFINITY); /* Numerical value of upper bound.*/

    for(j=vsize_state; j<vsize_state+vsize_state*ncount && r==MSK_RES_OK; ++j)
      r = MSK_putbound(task,
                       MSK_ACC_VAR,  /* Put bounds on variables.*/
                       j,            /* Index of variable.*/
                       MSK_BK_RA,    /* Bound key.*/
                       (double) 0.0, /* Numerical value of lower bound.*/
                       max_payout);  /* Numerical value of upper bound.*/

    for(j=vsize_state+vsize_state*ncount; j<vsize && r==MSK_RES_OK; ++j)
      r = MSK_putbound(task,
                       MSK_ACC_VAR,    /* Put bounds on variables.*/
                       j,              /* Index of variable.*/
                       MSK_BK_FR,      /* Bound key.*/
                       -MSK_INFINITY,  /* Numerical value of lower bound.*/
                       +MSK_INFINITY); /* Numerical value of upper bound.*/
    
    // Fill constraint Matrix A
    for(i=0; i<nnzA && r == MSK_RES_OK; ++i)
      r = MSK_putaij(task,
                     Ai[i]-1,
                     Aj[i]-1,
                     Aval[i]);

    /* Set the bounds on constraints.
       for i=1, ...,neqconstraints : blc[i] <= constraint i <= buc[i] */
    for(i=0; i<constraint_count && r==MSK_RES_OK; ++i) {
      if(optim_dir[i] == -1) {
        r = MSK_putbound(task,
                         MSK_ACC_CON, /* Put bounds on constraints.*/
                         i,           /* Index of constraint.*/
                         MSK_BK_UP,   /* Bound key.*/
                         -MSK_INFINITY,      /* Numerical value of lower bound.*/
                         b[i]);     /* Numerical value of upper bound.*/
      } else if(optim_dir[i] == 0) {
        r = MSK_putbound(task,
                         MSK_ACC_CON, /* Put bounds on constraints.*/
                         i,           /* Index of constraint.*/
                         MSK_BK_FX,   /* Bound key.*/
                         b[i],      /* Numerical value of lower bound.*/
                         b[i]);     /* Numerical value of upper bound.*/
      } else {
        r = MSK_putbound(task,
                         MSK_ACC_CON, /* Put bounds on constraints.*/
                         i,           /* Index of constraint.*/
                         MSK_BK_LO,   /* Bound key.*/
                         b[i],      /* Numerical value of lower bound.*/
                         MSK_INFINITY);     /* Numerical value of upper bound.*/
      }
    }

    // set the linear penalties
    for(j=0;j<vsize && r == MSK_RES_OK; ++j)
      r = MSK_putcj(task,j,c[j]);

    if ( r==MSK_RES_OK ) {
      // The lower triangular part of the Q
      // matrix in the objective is specified.
      for(j=0; j<vsize; ++j) {
        qsubi[j] = j;
        qsubj[j] = j;
        qval[j] = Qdiag[j];
      }
        
      /* Input the Q for the objective. */
      r = MSK_putqobj(task,vsize,qsubi,qsubj,qval);
    }

    if ( r==MSK_RES_OK )
      r = MSK_putobjsense(task, MSK_OBJECTIVE_SENSE_MINIMIZE);      

    if ( r==MSK_RES_OK ) {
      MSKrescodee trmcode;
      /* Run optimizer */
      r = MSK_optimizetrm(task,&trmcode);

        /* Print a summary containing information
           about the solution for debugging purposes*/
#ifndef NDEBUG
        MSK_solutionsummary (task,MSK_STREAM_LOG);
  #endif
        
        if ( r==MSK_RES_OK )
        {
          MSKsolstae solsta;
          
          MSK_getsolutionstatus (task,
                                 MSK_SOL_ITR,
                                 NULL,
                                 &solsta);
          
          switch(solsta)
          {
            case MSK_SOL_STA_OPTIMAL:   
            case MSK_SOL_STA_NEAR_OPTIMAL:
              MSK_getsolutionslice(task,
                                   MSK_SOL_ITR,    /* Request the interior solution. */
                                   MSK_SOL_ITEM_XX,/* Which part of solution.        */
                                   0,              /* Index of first variable.       */
                                   vsize,           /* Index of last variable+1.      */
                                   xx);
              for(j=0; j<vsize; ++j) {
                result[j] = xx[j];
              }
              
              break;
            case MSK_SOL_STA_DUAL_INFEAS_CER:
            case MSK_SOL_STA_PRIM_INFEAS_CER:
            case MSK_SOL_STA_NEAR_DUAL_INFEAS_CER:
            case MSK_SOL_STA_NEAR_PRIM_INFEAS_CER:  
              printf("Primal or dual infeasibility certificate found.\n");
              break;
              
            case MSK_SOL_STA_UNKNOWN:
              printf("The status of the solution could not be determined.\n");
              break;
            default:
              printf("Other solution status.");
              break;
          }
        }
        else
        {
          printf("Error while optimizing.\n");
        }
      }
    
      if (r != MSK_RES_OK)
      {
        /* In case of an error print error code and description. */      
        char symname[MSK_MAX_STR_LEN];
        char desc[MSK_MAX_STR_LEN];
        
        printf("An error occurred while optimizing.\n");     
        MSK_getcodedesc (r,
                         symname,
                         desc);
        printf("Error %s - '%s'\n",symname,desc);
      }
    }
 
    MSK_deletetask(&task);
  }
  MSK_deleteenv(&env);

  return ( r );
}
  
extern int solve_repo_refinement_linp(int steps, double start_loaded, int nnzA, int Ai[], int Aj[], double Aval[], double beq[], double c[], double result[])
{
  
  MSKrescodee  r;
  // We assume there is no linear cost term (set to 0 in code)

  MSKidxt      qsubi[steps];
  MSKidxt      qsubj[steps];
  double       qval[steps];
  
  MSKidxt      j,i;
  double       xx[5*steps];
  MSKenv_t     env;
  MSKtask_t    task;

  /* Create the mosek environment. */
  r = MSK_makeenv(&env,NULL,NULL,NULL,NULL);

  /* #ifndef NDEBUG */
  /* /\* Check whether the return code is ok. *\/ */
  /* if ( r==MSK_RES_OK ) */
  /* { */
  /*   /\* Directs the log stream to the 'printstr' function. *\/ */
  /*   MSK_linkfunctoenvstream(env,MSK_STREAM_LOG,NULL,printstr); */
  /* } */
  /* #endif */

  /* Initialize the environment. */   
  r = MSK_initenv(env);
  if ( r==MSK_RES_OK )
  {   
  /* Create the optimization task. */
    r = MSK_maketask(env,7*steps,5*steps,&task);

    if ( r==MSK_RES_OK ) {
      
    /* Give MOSEK an estimate of the size of the input data. 
     This is done to increase the speed of inputting data. 
     However, it is optional. */
    if (r == MSK_RES_OK)
      r = MSK_putmaxnumvar(task,5*steps);
  
    if (r == MSK_RES_OK)
      r = MSK_putmaxnumcon(task,7*steps);
    
    if (r == MSK_RES_OK)
      r = MSK_putmaxnumanz(task,nnzA);

    /* Append 'nvar' variables.
     The variables will initially be fixed at zero (x=0). */
    if ( r == MSK_RES_OK )
      r = MSK_append(task,MSK_ACC_VAR,5*steps);

    /* Append 'neqconstraints' empty constraints.
     The constraints will initially have no bounds. */
    if ( r == MSK_RES_OK )
      r = MSK_append(task,MSK_ACC_CON,7*steps);

    /* /\* Set the bounds on variable j. */
    for(j=0; j<5*steps && r==MSK_RES_OK; ++j)
      r = MSK_putbound(task,
                       MSK_ACC_VAR,    /* Put bounds on variables.*/
                       j,              /* Index of variable.*/
                       MSK_BK_FR,      /* Bound key.*/
                       -MSK_INFINITY,  /* Numerical value of lower bound.*/
                       +MSK_INFINITY); /* Numerical value of upper bound.*/

    // Fill constraint Matrix A
    for(i=0; i<nnzA && r == MSK_RES_OK; ++i)
      r = MSK_putaij(task,
                     Ai[i]-1,
                     Aj[i]-1,
                     Aval[i]);

    /* Set the bounds on constraints. */
    for(i=0; i<steps && r==MSK_RES_OK; ++i)
        r = MSK_putbound(task,
                         MSK_ACC_CON, /* Put bounds on constraints.*/
                         i,           /* Index of constraint.*/
                         MSK_BK_RA,   /* Bound key.*/
                         (double) -start_loaded,      /* Numerical value of lower bound.*/
                         (double) 20.0-start_loaded);     /* Numerical value of upper bound.*/

    for(i=steps; i<steps*3 && r==MSK_RES_OK; ++i)
        r = MSK_putbound(task,
                         MSK_ACC_CON, /* Put bounds on constraints.*/
                         i,           /* Index of constraint.*/
                         MSK_BK_FX,   /* Bound key.*/
                         beq[i-steps],      /* Numerical value of lower bound.*/
                         beq[i-steps]);     /* Numerical value of upper bound.*/
    
    for(i=steps*3; i<steps*7 && r==MSK_RES_OK; ++i)
        r = MSK_putbound(task,
                         MSK_ACC_CON, /* Put bounds on constraints.*/
                         i,           /* Index of constraint.*/
                         MSK_BK_UP,   /* Bound key.*/
                         (double) 0.0,      /* Numerical value of lower bound.*/
                         (double) 0.0);     /* Numerical value of upper bound.*/

    for(j=0;j<5*steps && r == MSK_RES_OK; ++j)
      r = MSK_putcj(task,j,c[j]);

    // very small quadratic penalty for delta_bikes. we want to take as little bikes as possible.
    // 0.00001 such that it won't influence "real" decisions
    if ( r==MSK_RES_OK ) {
      // The lower triangular part of the Q
      // matrix in the objective is specified.
      for(j=0; j<steps; ++j) {
        qsubi[j] = j;
        qsubj[j] = j;
        qval[j] = (double) 0.0001;
      }
        
      /* Input the Q for the objective. */
      r = MSK_putqobj(task,steps,qsubi,qsubj,qval);
    }
    
    /* /\* Specify integer variables. *\/ */
    /* for(j=0; j<steps && r == MSK_RES_OK; ++j) */
    /*   r = MSK_putvartype(task,j,MSK_VAR_TYPE_INT); */
    
    if ( r==MSK_RES_OK )
      r = MSK_putobjsense(task, MSK_OBJECTIVE_SENSE_MINIMIZE);      

    if ( r==MSK_RES_OK ) {
      MSKrescodee trmcode;
      /* Run optimizer */
      r = MSK_optimizetrm(task,&trmcode);

/*         /\* Print a summary containing information */
/*            about the solution for debugging purposes*\/ */
/* #ifndef NDEBUG */
/*       MSK_solutionsummary (task,MSK_STREAM_LOG); */
/*   #endif */
        
        if ( r==MSK_RES_OK )
        {
          MSKsolstae solsta;
          
          MSK_getsolutionstatus (task,
                                 MSK_SOL_ITR,
                                 NULL,
                                 &solsta);
          
          switch(solsta)
          {
            case MSK_SOL_STA_OPTIMAL:
            case MSK_SOL_STA_NEAR_OPTIMAL:
              MSK_getsolutionslice(task,
                                   MSK_SOL_ITR,    /* Request the interior solution. */
                                   MSK_SOL_ITEM_XX,/* Which part of solution.        */
                                   0,              /* Index of first variable.       */
                                   5*steps,           /* Index of last variable+1.      */
                                   xx);
              for(j=0; j<5*steps; ++j) {
                result[j] = xx[j];
              }
              
              break;
            case MSK_SOL_STA_DUAL_INFEAS_CER:
            case MSK_SOL_STA_PRIM_INFEAS_CER:
            case MSK_SOL_STA_NEAR_DUAL_INFEAS_CER:
            case MSK_SOL_STA_NEAR_PRIM_INFEAS_CER:
              printf("Primal or dual infeasibility certificate found.\n");
              break;
              
            case MSK_SOL_STA_UNKNOWN:
              printf("The status of the solution could not be determined.\n");
              break;
            default:
              printf("Other solution status.");
              break;
          }
        }
        else
        {
          printf("Error while optimizing.\n");
        }
      }
    
      if (r != MSK_RES_OK)
      {
        /* In case of an error print error code and description. */      
        char symname[MSK_MAX_STR_LEN];
        char desc[MSK_MAX_STR_LEN];
        
        printf("An error occurred while optimizing.\n");     
        MSK_getcodedesc (r,
                         symname,
                         desc);
        printf("Error %s - '%s'\n",symname,desc);
      }
    }
 
    MSK_deletetask(&task);
  }
  MSK_deleteenv(&env);

  return ( r );
} /* main */
  
