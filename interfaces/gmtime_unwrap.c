// Mac: gcc -Wall gmtime_unwrap.c -O3 -DNDEBUG -dynamiclib -o gmtime_unwrap.dylib
// Linux: gcc -shared -Wall -fPIC gmtime_unwrap.c -o gmtime_unwrap.so

#include <time.h>

#define generate_accessors(field)                 \
  extern int _jl_time__tm_get_ ## field(void * v) \
  {                                               \
    struct tm * s = (struct tm *) v;              \
    return s->tm_ ## field;                       \
  }

generate_accessors(sec);
generate_accessors(min);
generate_accessors(hour);
generate_accessors(mday);
generate_accessors(mon);
generate_accessors(year);
generate_accessors(wday);
generate_accessors(yday);
generate_accessors(isdst);
