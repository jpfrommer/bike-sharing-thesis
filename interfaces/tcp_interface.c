// gcc -shared -Wall -fPIC tcp_interface.c -o tcp_interface.so
// gcc -Wall tcp_interface.c -O3 -DNDEBUG -dynamiclib -o tcp_interface.dylib
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>

extern int createServerAndListen(int port)
{
    struct sockaddr_storage their_addr;
    socklen_t addr_size;
    struct addrinfo hints, *res;
    int sockfd, new_fd;

    // first, load up address structs with getaddrinfo():

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;  // use IPv4 or IPv6, whichever
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;     // fill in my IP for me
    char portstring[8];
    sprintf(portstring, "%d", port);
    getaddrinfo(NULL, portstring, &hints, &res);

    // make a socket, bind it, and listen on it:

    sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    bind(sockfd, res->ai_addr, res->ai_addrlen);
    
    int listenresult = listen(sockfd, 5);
    if(listenresult == -1)
      return -1;

    // now accept an incoming connection:
    addr_size = sizeof their_addr;
    new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &addr_size);

    return new_fd;
}

extern int sendMessage(char msg[], int msglen, int connectionfd) {
  int sendlen;

  int index = 0;
  
  while(1) {
    sendlen = send(connectionfd, msg+index, msglen, 0);
    if(sendlen == -1) { return 0; }
    
    if(sendlen != msglen) {
      index = index+sendlen;
      msglen = msglen-sendlen;
    }
    break;
  }
  return 1;
}


extern int receiveMessage(char buf[], int buflen, int connectionfd)
{
  int n = recv(connectionfd,buf,buflen,0);
  return n;
};

extern int closeServer(int connfd)
{
  return close(connfd);
};
