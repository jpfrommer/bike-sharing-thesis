load("system.jl")

# The simplified linear model takes into regard only the incentives to the station to which we want to direct the customer
# NOT the incentives to the other neighbours. Therefore the resulting cost function has only the diagonal filled
function calc_linear_incentive_influence(sys::BikeSystem)
    max_payout = 4.0 # in euro
    inc_prob = zeros(Float64,sys.stationcount,sys.stationcount*sys.neighbourcount)

    # 1) For each station
    for s=1:sys.stationcount
        println(s)

        # No dead stations
        if sys.stations[s].free + sys.stations[s].bikes == 0
            continue
        end

        # The combined distance from the Voronoi partition
        distances = Array(Float,sys.neighbourcount)
        for n=1:sys.neighbourcount
            distances[n] = sys.neighbours[s,n].distance
        end

        # 2) Create Samples.
        # payouts is matrix: i = sample, j = payouts
        # ip is a matrix also: i = sample, j = probability of arriving customer to go to j-neighbour
        payouts,ip = create_incentive_samples(distances, max_payout)

        # 3.1) Compute the probability depending on all 10 neighbour payouts
        # the probability for an incentive to be taken from n to s.
        # depends on the payout from n and all neighbours of n: n2
        for n=1:sys.neighbourcount
            linmodel = linear_regression(float64(payouts),float64(ip[:,n])) # how do the N payouts to the neighbours of s influence the percentage of customers taking s-n
            neighbourid = sys.neighbours[s,n].neighbourid
            inc_prob[neighbourid,sys.neighbourcount*(s-1)+1:sys.neighbourcount*(s-1)+sys.neighbourcount] = linmodel'
            inc_prob[s,sys.neighbourcount*(s-1)+1:sys.neighbourcount*s] = inc_prob[s,sys.neighbourcount*(s-1)+1:sys.neighbourcount*s] -linmodel'
        end

        # 3.2) Compute the probability based only on the payout to the station
        # for n=1:sys.neighbourcount
        #     neighbourid = sys.neighbours[s,n].neighbourid
        #     direct_payouts = payouts[:,n]
        #     probability = ip[:,n]
        #     linear_influence = sum(direct_payouts .* probability) / sum(direct_payouts .^ 2)
        #     inc_prob[neighbourid,sys.neighbourcount*(s-1)+n] = linear_influence
        #     inc_prob[s,sys.neighbourcount*(s-1)+n] = -linear_influence
        # end
    end
    return inc_prob
end

## return values:
# payouts::Matrix{Float}. Every row is a set of payouts
# ip::Matrix{Float}. Incentive probabilities.
# Every i contains the probability of a new customer to go to station j when the payouts from payouts_i are offered
function create_incentive_samples(distances::Vector{Float}, max_payout)
    ncount = length(distances)
    samplecount = 2000
    payouts = Array(Float,samplecount,ncount)
    ip = Array(Float,samplecount,ncount)
    for i=1:samplecount
        thispayout = rand(ncount) * max_payout
        payouts[i,:] = thispayout'
        ip[i,:] = create_incentive_sample(thispayout,distances)'
    end
    return payouts, ip
end

# input is payout and distance to the neighbours
# result is the probability for the customer to take a certain incentive based on p and d
function create_incentive_sample(payouts::Vector{Float64}, distances::Vector{Float})
    ncount = length(payouts)
    probabilities = zeros(Float,ncount)
    T = 1000
    for i = 1:T
        customer_distance_costs = realize_customer_distance_costs()
        best_neighbour_station = 0
        max_value = -Inf
        for n = 1:ncount
            value = payouts[n] - (distances[n] * customer_distance_costs)
           # value = value  + randn()*0.25*value # ~25% of the perceived value are random
            if value > max_value
                max_value = value
                best_neighbour_station = n
            end
        end
        if max_value > 0
            probabilities[best_neighbour_station] =  probabilities[best_neighbour_station] + 1.0
        end
    end
    probabilities = probabilities / float(T)
    return probabilities
end

# we can change the distribution of how customers value their time here
function realize_customer_distance_costs()
    max_customer_distance_value = 20 # in euro per kilometer
    return rand() * max_customer_distance_value
end

# Linear Regression with SVD (from http://www.statalgo.com/2012/04/27/statistics-with-julia-least-squares-regression-with-direct-methods/)
function linear_regression(payouts::Matrix{Float64},ip::Vector{Float64})
    m,n = size(payouts);
    # Q,R = qr(payouts);
    # R1 = R[1:n, 1:n];
    # Q1 = Q[:, 1:n];
    # beta = R1 \ (Q1' * ip)

    (U, S, V) = svd(payouts);
    c = U' * ip;
    c1 = c[1:n];
    c2 = c[n+1:m];
    z1 = c1 ./ S;
    beta = V' * z1
    
    # quality
    # tss = sum((ip - mean(ip)).^2)
    # rss = sum((ip - (payouts * beta)).^2)
    # println(sqrt(1 - (rss/tss))) # print correlation coefficient
    ##
    
    return beta
end
