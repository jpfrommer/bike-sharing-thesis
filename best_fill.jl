# load("system.jl") before that

type BestFillStatus
    plateau_start::Float
    plateau_end::Float
    best_amount::Float
end

function copy(bf::BestFillStatus)
    return BestFillStatus(copy(bf.plateau_start), copy(bf.plateau_end), copy(bf.best_amount))
end

type BestFillCache
    workday::Array{BestFillStatus,2}
    friday::Array{BestFillStatus,2}
    saturday::Array{BestFillStatus,2}
    sunday::Array{BestFillStatus}
end

function readBestFillCache()
    workday = dlmread("system_description/best_fill_workday.txt",",",Float32)
    s_max = int(size(workday)[1]/3)
    t_max = size(workday)[2]
    best_fill_workday = Array(BestFillStatus,s_max,t_max)
    for t=1:t_max, s=1:s_max
        best_fill_workday[s,t] = BestFillStatus(workday[s,t],workday[s+s_max,t],workday[s+2*s_max,t])
    end

    friday = dlmread("system_description/best_fill_friday.txt",",",Float32)
    best_fill_friday = Array(BestFillStatus,s_max,t_max)
    for t=1:t_max, s=1:s_max
        best_fill_friday[s,t] = BestFillStatus(friday[s,t],friday[s+s_max,t],friday[s+2*s_max,t])
    end

    saturday = dlmread("system_description/best_fill_saturday.txt",",",Float32)
    best_fill_saturday = Array(BestFillStatus,s_max,t_max)
    for t=1:t_max, s=1:s_max
        best_fill_saturday[s,t] = BestFillStatus(saturday[s,t],saturday[s+s_max,t],saturday[s+2*s_max,t])
    end
    
    sunday = dlmread("system_description/best_fill_sunday.txt",",",Float32)
    best_fill_sunday = Array(BestFillStatus,s_max,t_max)
    for t=1:t_max, s=1:s_max
        best_fill_sunday[s,t] = BestFillStatus(sunday[s,t],sunday[s+s_max,t],sunday[s+2*s_max,t])
    end
    
    return BestFillCache(best_fill_workday, best_fill_friday, best_fill_saturday, best_fill_sunday)
end

function cumulatedFutureNetArrivalMatrix(sys::BikeSystem, weekend::Bool, T::Int)
    cna = Array(Float,sys.stationcount,24*12)
    for s=1:sys.stationcount
        println(s)
        for t=1:24*12
        cna[s,t] = cumulatedFutureNetArrival(sys,s,t,t+T,weekend)
        end
    end
    return cna
end

function cumulatedFutureNetArrival(sys::BikeSystem,s::Int, t_5::Int, T_5::Int, weekend::Bool)
    cum_net_arrival = 0.0
    for step = t_5:min(287,T_5)
        t = floor(step/4)+1 # arrival is in 20 min steps
        if weekend
            cum_net_arrival += sys.behavior.arrival_weekend_sum[s,t] * 5
            cum_net_arrival -= sys.behavior.departure_weekend_sum[s,t] * 5
        else
            cum_net_arrival += sys.behavior.arrival_workday_sum[s,t] * 5
            cum_net_arrival -= sys.behavior.departure_workday_sum[s,t] * 5
        end
    end
    return cum_net_arrival
end

function writeBestFillCache(bf::Array{BestFillStatus,2}, name::String)
    bigmatrix = Array(Float,size(bf,1)*3, size(bf,2))
    s_count = size(bf,1)
    for s = 1:s_count, t=1:size(bf,2)
        bigmatrix[s,t] = bf[s,t].plateau_start
        bigmatrix[s+s_count,t] = bf[s,t].plateau_end
        bigmatrix[s+2*s_count,t] = bf[s,t].best_amount
    end
    dlmwrite(name, bigmatrix)
end

# compute the best fill status in 5 min steps
# horizon must be below 24*12*2
function computeBestFillStatus(sys::BikeSystem, daytype::Int, horizon::Int)
    best_fill = Array(BestFillStatus,sys.stationcount,72*4)
    for s=1:sys.stationcount
        println(s)
        if daytype == 1 || daytype == 2
            firstday_net_arrival = sys.behavior.arrival_workday_sum[s,:] - sys.behavior.departure_workday_sum[s,:]
        else
            firstday_net_arrival = sys.behavior.arrival_weekend_sum[s,:] - sys.behavior.departure_weekend_sum[s,:]
        end
        if daytype == 1 || daytype == 4
            secondday_net_arrival = sys.behavior.arrival_workday_sum[s,:] - sys.behavior.departure_workday_sum[s,:]
        else
            secondday_net_arrival = sys.behavior.arrival_weekend_sum[s,:] - sys.behavior.departure_weekend_sum[s,:]
        end
        net_arrival = cat(1,firstday_net_arrival', secondday_net_arrival') * 5.0 # from 1 minute lambda to 20 min

        long_net_arrival = Array(Float64,144*4) # arrival over the next two days
        for t = 1:24*12*2-1
            long_net_arrival[t] = net_arrival[floor(t/4.0)+1]
        end

        for t = 1:24*12
            best_fill[s,t] = findBestFillStatus(sys, long_net_arrival,s,t, horizon)
        end
    end
    return best_fill
end

## What is happending if we start at start_fine_slice at the exact midpoint and customers behave as expected?
# We have 4 best_fill matrices: 1) On a normal workday, 2) on a friday, on 3) saturday and 4) on sunday,
# because we consider only the "morning after" to be important
# we get rid of the night period between 1 and 6, where no bikes arrive.
# To compute the best_fill, we take a 48h horizon. The last index can thus "see" the whole next day. In most cases, the utility algo should return much sooner
# fine slices in 5 min periods
# t in 20 min steps
function findBestFillStatus(sys::BikeSystem, net_arrival::Array{Float64,1}, s::Int64, t::Int64, horizon::Int64)

    max_bikes = float(sys.stations[s].total)
    midpoint = sys.stations[s].total / 2.0
    
    # What will happen if arrival is like expected
    station_fill = Array(Float,24*12*2) # 2 days
    station_fill[t] = midpoint
    for index = t:24*12*2-1
        station_fill[index+1] = max(0.0, min(station_fill[index]+net_arrival[index], max_bikes))
    end

    # 1) Change in utility if we fill the station to the max
    max_bikes_utility = utilityOfChangedFillStatus(station_fill, net_arrival, t, t+horizon, max_bikes, max_bikes)
    if abs(max_bikes - midpoint - max_bikes_utility) < 0.1
        if sys.stations[s].total != 0
            return BestFillStatus(max_bikes, max_bikes, max_bikes) # fill up is best, no plateau
        else
            return BestFillStatus(0.0, 0.0, 0.0) # dead station
        end
    end

    # 2) Change in utility if we take away all bikes
    min_bikes_utility = utilityOfChangedFillStatus(station_fill, net_arrival, t, t+horizon, 0.0, max_bikes)
    if abs(midpoint - min_bikes_utility) < 0.01
        return BestFillStatus(0.0, 0.0, 0.0) # remove all is best, no plateau
    end

    # 3) Both are not optimal. There must be a peak or plateau in the middle
    peak_bikes = (max_bikes_utility - min_bikes_utility + max_bikes) / 2.0
    peak_bikes_utility = utilityOfChangedFillStatus(station_fill, net_arrival, t, t+horizon, peak_bikes, max_bikes)

    plateau_start = 0.0
    if abs(peak_bikes_utility - min_bikes_utility) > 0.1
        plateau_start = peak_bikes_utility - min_bikes_utility
    end

    plateau_end = max_bikes
    if abs(peak_bikes_utility - max_bikes_utility) > 0.1
        plateau_end = max_bikes - (peak_bikes_utility - max_bikes_utility)
    end
    
    return BestFillStatus(plateau_start, plateau_end, (plateau_start+plateau_end) / 2.0)
end

# recursive version
function utilityOfChangedFillStatus(station_fill::Array{Float,1}, net_arrival::Array{Float64,1}, t::Int64, T::Int64, new_bike_amount::Float64, max_bikes::Float64)
    # no change case
    if abs(new_bike_amount-station_fill[t]) < 0.001 || T <= t
        return 0.0
    end
    # what is the utility in this period? Will we run empty/full without an arriving truck?
    following_new_bike_amount = max(0, min(new_bike_amount+net_arrival[t], max_bikes))
    orig_usage = abs(station_fill[t] - station_fill[t+1])
    new_usage = abs(new_bike_amount - following_new_bike_amount)
    # what is the utility starting with the next period? (recursion)
    return new_usage - orig_usage + utilityOfChangedFillStatus(station_fill, net_arrival, t+1, T, following_new_bike_amount, max_bikes)
end

# explicit version based on the cache
function changedFillUtility(bfs::BestFillStatus, orig_amount::Float, new_amount::Float)
    if orig_amount <= bfs.plateau_start
        orig_utility = orig_amount - bfs.plateau_start
    elseif orig_amount >= bfs.plateau_end
        orig_utility = bfs.plateau_end - orig_amount
    else
        orig_utility = 0.0
    end

    if new_amount <= bfs.plateau_start
        new_utility = new_amount - bfs.plateau_start
    elseif new_amount >= bfs.plateau_end
        new_utility = bfs.plateau_end - new_amount
    else
        new_utility = 0.0
    end

    return new_utility - orig_utility
end

# check wether the result of the best_fill matrix always give the correct utility. Implementation test
function testBestFill(sys::BikeSystem, daytype::Int, best_fill::Array{BestFillStatus,2})
    while true
        s = randi(sys.stationcount)
        if daytype == 1 || daytype == 2
            firstday_net_arrival = sys.behavior.arrival_workday_sum[s,:] - sys.behavior.departure_workday_sum[s,:]
        else
            firstday_net_arrival = sys.behavior.arrival_weekend_sum[s,:] - sys.behavior.departure_weekend_sum[s,:]
        end
        if daytype == 1 || daytype == 4
            secondday_net_arrival = sys.behavior.arrival_workday_sum[s,:] - sys.behavior.departure_workday_sum[s,:]
        else
            secondday_net_arrival = sys.behavior.arrival_weekend_sum[s,:] - sys.behavior.departure_weekend_sum[s,:]
        end
        net_arrival = cat(1,firstday_net_arrival', secondday_net_arrival') * 20.0 # from 1 minute lambda to 20 min
        
        long_net_arrival = Array(Float,144*4)
        for t = 1:144*4
            long_net_arrival = net_arrival[floor(t/4.0)+1]
        end

        t = randi(24*12)
        orig_bike_amount = rand()*sys.stations[s].total
        new_bike_amount = rand()*sys.stations[s].total
        
        station_fill = Array(Float,144*4) # 2 days
        station_fill[t] = orig_bike_amount
        for index = t:144*4-1
            station_fill[index+1] = max(0.0, min(station_fill[index]+long_net_arrival[index], sys.stations[s].total))
        end

        algo_utility = utilityOfChangedFillStatus(station_fill, long_net_arrival[:,1], t, 144, new_bike_amount, float(sys.stations[s].total))
        precomputed_utility = changedFillUtility(best_fill[s,t], orig_bike_amount, new_bike_amount)

        if abs(precomputed_utility - algo_utility) > 0.1
            println("failure! results don't match.")
        else
            println("hooray")
        end

    end
end

# positive value = bring, negative value = take
function bestAction(best_fill::Array{BestFillStatus,2}, s::Int, t_5_global::Int, current_sv::Float)
    bf = best_fill[s,t_5_global]
    if current_sv < bf.plateau_start
        return int(floor(bf.plateau_start - current_sv))
    elseif current_sv > bf.plateau_end
        return int(ceil(bf.plateau_end - current_sv))
    else # bf.plateau_start <= current_sv <= bf.plateau_end
        return 0
    end
end
