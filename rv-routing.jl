# Routing of repositioning vehicles

type StationTimeGraph
    # state vertices are implicit
    deadp::Matrix{Bool} # can this vertex still reach the depot in time?
    bikes::Matrix{Float32} # amount of bikes present at the end of each state
    net_arrival::Matrix{Float32} # what would happen in this phase if bikes/space were available?
end

type Routing
    departure::Array{Int,2} # in epoch_time. i: vehicle, j: nth-station. If this is zero, we are at the end of the path for this truck
    stations::Array{Int,2} # where is the truck at the time
    bikes::Array{Int,2} # how many bikes are being picked up? (unloading is handled automatically since we generate Trips in the simulation)
end

function greedyRepositioningRouting(sys::BikeSystem, bfc::BestFillCache, epoch_time::Int64, horizon::Int64, return_to_depot::Bool, start_stations::Vector{Int64}, start_time::Vector{Int64})
    stg = createStationTimeGraph(sys,epoch_time,horizon,return_to_depot)
    routes = greedyRepositioningRouteSearch(sys, stg, bfc, epoch_time, horizon, return_to_depot, start_stations, start_time)
    rv_amount = length(start_stations)
    maxlength = 0
    for i = 1:rv_amount
        if length(routes[i]) > maxlength
            maxlength = length(routes[i])
        end
    end
    departure_time = zeros(Int, length(routes), maxlength+1)
    stations = zeros(Int, length(routes), maxlength+1)
    bikes = zeros(Int, length(routes), maxlength+1)
    departure_time[:,1] = start_time
    stations[:,1] = start_stations
    for i = 1:rv_amount
        for p = 2:maxlength+1
            if p > length(routes[i])+1
                break
            end
            target = routes[i][p-1]
            departure_time[i,p] = departure_time[i,p-1] + sys.transition_time_class[stations[i,p-1],target[1]] * 300
            stations[i,p] = target[1]
            bikes[i,p] = target[2]
        end
    end
    return Routing(departure_time, stations, bikes)
end

# horizon in 5 min steps
# start pos where the truck just loaded off bikes. the trucks are empty at this point
function greedyRepositioningRouteSearch(sys::BikeSystem, stg::StationTimeGraph, bfc::BestFillCache, epoch_time::Int, horizon::Int, return_to_depot::Bool, start_pos::Vector{Int}, start_time::Vector{Int})
    rv_amount = length(start_pos)
    curr_pos = copy(start_pos) # copy since we do not want to modify the original
    
    routes = cell(rv_amount) # one route for each repositionig vehicles
    for i=1:rv_amount
        routes[i] = Array(Any,0)
    end
    
    current_t = 1 # in 5 min steps
    time_remaining = int(floor((start_time - epoch_time) / 300)) # in how many 5 minute steps are the trucks starting?

    while true
        # 0) Whis is the next truck who has finished his tour (first start or end of 2-step)
        step = min(time_remaining)
        epoch_time = int(epoch_time + step * 300)
        time_remaining = time_remaining - step
        current_t = current_t + step # time in horizon in 5 min steps
        curr_rv = findmin(time_remaining)[2]
        current_s = curr_pos[curr_rv]
        
        if current_t > horizon
            break
        end
        
        date = gmtime(epoch_time)
        if date.wday == 0
            best_fill = bfc.sunday
        elseif date.wday == 5
            best_fill = bfc.friday
        elseif date.wday == 6
            best_fill = bfc.saturday
        else
            best_fill = bfc.workday
        end

        # At the beginning, we are always "empty". Search for the best run consisting of driving somewhere to take bikes and driving somewhere to put them there
        best_move = (0,0) # format is (station, take_bikes)
        best_move_bikes = 0
        best_move_value = 0

        # 1) Find out the best stations for taking bikes away.
        take_utilities = zeros(Float, sys.stationcount)
        for s=1:sys.stationcount
            if current_t+sys.transition_time_class[current_s,s] > horizon
                continue
            end
            arrival_t = current_t+sys.transition_time_class[current_s,s]
            take_utilities[s] = bestAction(best_fill[s,arrival_t], stg.bikes[s,arrival_t], true, 20) # what is at each station the best amount I can take away?
        end
        take_utilities_per_distance = take_utilities ./ sys.transition_time_class[current_s,:]
        sorted_tupd,sorted_tupd_id = sortperm(take_utilities_per_distance)

        # 2) Find the best stations to put them
        for i=1:10
            taken_station = sorted_tupd_id[sys.stationcount+1-i]
            taken_bikes = take_utilities[taken_station]
            time_at_take = sys.transition_time_class[current_s,taken_station]
            for s=1:sys.stationcount
                total_transition_time = time_at_take+sys.transition_time_class[taken_station,s]
                arrival_t = current_t + total_transition_time
                if arrival_t > horizon
                    continue
                end
                best_bring_action = bestAction(best_fill[s,arrival_t], stg.bikes[s,arrival_t], false, 20)
                moved_bikes = min(taken_bikes, best_bring_action)
                if moved_bikes/total_transition_time > best_move_value && !(return_to_depot && stg.deadp[s,arrival_t])
                    # won't cut it if we cannot bring all the bikes or cannot return in the end
                    best_move = (taken_station,s)
                    best_move_value = moved_bikes/total_transition_time
                    best_move_bikes = moved_bikes
                end
            end
        end

        # 3) Write results. Also adapt the stg
        if best_move_value > 0
            push(routes[curr_rv],(best_move[1], best_move_bikes)) # we take the bikes from here
            push(routes[curr_rv],(best_move[2], 0)) # next ride is empty
            updateStationTimeGraph(sys, stg, epoch_time, best_move[1], current_t+sys.transition_time_class[current_s,best_move[1]], take_utilities[best_move[1]])
            updateStationTimeGraph(sys, stg, epoch_time, best_move[2], current_t+sys.transition_time_class[current_s,best_move[1]]+sys.transition_time_class[best_move[1],best_move[2]], take_utilities[best_move[1]])
            time_remaining[curr_rv] = sys.transition_time_class[curr_pos[curr_rv],best_move[1]]+sys.transition_time_class[best_move[1],best_move[2]]
            curr_pos[curr_rv] = best_move[2]
        else
            if current_t + 6 >= horizon && return_to_depot # it is implicit that there are not good "alive" stations to go to
                push(routes[curr_rv],(346,0))
                time_remaining[curr_rv] = 99999999
            end
        end
    end
    return routes
end

# the graph itself starts off at starttime=1
function createStationTimeGraph(sys::BikeSystem, starttime::Int, horizon::Int, return_to_depot::Bool)
    stg = StationTimeGraph(zeros(Bool,sys.stationcount, horizon+1), zeros(Float32,sys.stationcount, horizon+1), zeros(Float32,sys.stationcount, horizon+1))

    # 1) Fill the stg with the expected bikes at each station without repositioning
    # and save the net arrival that might be realized
    for s=1:sys.stationcount
        stg.bikes[s,1] = sys.stations[s].bikes
    end

    for t=1:horizon
        time = int(starttime+(t-1)*300)
        net_arrival = netArrival(sys,time)
        for s=1:sys.stationcount
            stg.bikes[s,t+1] = max(0, min(stg.bikes[s,t]+net_arrival[s], sys.stations[s].bikes+sys.stations[s].free))
        end
        stg.net_arrival[:,t+1] = net_arrival
    end

    # 2) Mark dead vertices from which the depot cannot be reached in time
    if return_to_depot
        stg.deadp[:,horizon+1] = ones(Bool,sys.stationcount)
        stg.deadp[346,horizon+1] = false # 346 is the depot .. hardcoded

        for i = 1:6
            # every RV can reach the depot if it has more than 1h
            # we assume that if there is no fast enough direct connection, there is also none leading over another station
            stg.deadp[:,horizon+1-i] = sys.transition_time_class[:346] .<= i
            stg.deadp[346,horizon+1-i] = false
        end
    end
    return stg
end

function updateStationTimeGraph(sys::BikeSystem, stg::StationTimeGraph, starttime::Int, change_s::Int, change_t::Int, new_bike_amount::Float)
    stg.bikes[change_s,change_t] = new_bike_amount
    for t=change_t:(size(stg.bikes)[2])-1
        time = int(starttime+(t-1)*300)
        net_arrival = netArrival(sys, time)
        stg.bikes[change_s,t+1] = max(0, min(stg.bikes[change_s,t]+net_arrival[change_s], sys.stations[change_s].bikes+sys.stations[change_s].free))
    end
end

function netArrival(sys::BikeSystem, time::Int)
    date = gmtime(time)
    slice = current_slice(date,sys.behavior.slices)
    if isbusday(date)
        net_arrival = sys.behavior.arrival_workday_sum[:,slice] - sys.behavior.departure_workday_sum[:,slice]
    else
        net_arrival = sys.behavior.arrival_weekend_sum[:,slice] - sys.behavior.departure_weekend_sum[:,slice]
    end
    return net_arrival
end

function bestAction(best_fill::Array{BestFillStatus,2}, s::Int, t_5::Int, current_sv::Float)
    bf = best_fill[s,t_5]
    if current_sv < bf.plateau_start
        return min(bf.plateau_start - current_sv, 20.0)
    end

    if current_sv > bf.plateau_end
        return max(bf.plateau_end-current_sv, -20)
    end

    return 0.0
end