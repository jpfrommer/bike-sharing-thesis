from pymongo.connection import Connection
import json
import datetime
from bottle import Bottle, run, debug, static_file
from socket import *
import csv
import time
import platform

host = "localhost"
port = 9998
buf = 4096
addr = (host,port)
# Create socket and bind to address
sock = socket(AF_INET,SOCK_STREAM)
sock.connect(addr)
sock.settimeout(None)
connection = Connection("localhost")
db = connection['bikedata']

app = Bottle()

@app.route('/<filename>')
def callback(filename):
    if platform.system() == 'Linux':
        return static_file(filename, root='/home/julius/Documents/bike-sharing-thesis/visualization/web-ui/')
    return static_file(filename, root='/Users/julius/Documents/Uni/Diplomarbeit/bike-sharing-thesis/visualization/web-ui/')

@app.route('/stations')
def index():
    stations_cursor = db['stations'].find()
    stations_cursor.sort("_id", 1)
    stations = []
    for station in stations_cursor:
        stations.append(station)
    return json.dumps(stations, sort_keys=True, indent=4)

@app.route('/newbikes/<date_ms:int>/step/<duration_ms:int>')
def index(date_ms, duration_ms):
    date_end = datetime.datetime.utcfromtimestamp(date_ms)
    date_start = datetime.datetime.utcfromtimestamp(date_ms-duration_ms)
    
    rides_cursor = db['bikedata'].find({"start-date" : {"$gte" : date_start, "$lt" : date_end }})
    rides = []
    for ride in rides_cursor:
        rides.append(ride)
 
    dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else None
    return json.dumps(rides, default=dthandler)

@app.route('/update_world/<date_sec:int>/<control_param:int>')
def index(date_sec, control_param):
    sock.sendall(str(date_sec) + " " + str(control_param) +"\n")
    new_bikes_str = []
    new_truck_str = []
    incentives_str = []
    stations_status_str = []
    socketfile = sock.makefile()
    mode = 1 # get new bikes/get new trucks/get incentives/get station status
    while 1:
        line = socketfile.readline()
        if len(line) == 1:
            mode = mode+1
            if mode > 4:
                break
            continue
        if mode == 1:
            new_bikes_str.append(line.strip())
        elif mode == 2:
            new_truck_str.append(line.strip())
        elif mode == 3:
            incentives_str.append(line.strip())
        else:
            stations_status_str.append(line.strip())
   
    stations_status = []
    if len(stations_status_str) > 0:
        for row in csv.reader(stations_status_str):
            station = dict()
            station['bikes'] = int(row[0])
            station['spaces'] = int(row[1])
            station['broken'] = 0
            stations_status.append(station)
 
    new_bikes = []
    if len(new_bikes_str) > 0:
        for row in csv.reader(new_bikes_str):
            bike = dict()
            bike['start-station'] = int(row[0])
            bike['end-station'] = int(row[1])
            ride_duration = int(float(row[2]))
            bike['duration'] = ride_duration
            bike['start-date'] = datetime.datetime.utcfromtimestamp(date_sec)
            bike['end-date'] = datetime.datetime.utcfromtimestamp(date_sec+ride_duration)
            new_bikes.append(bike)
            
    new_trucks = []
    if len(new_truck_str) > 0:
        for row in csv.reader(new_truck_str):
            truck = dict()
            truck['start-station'] = int(row[0])
            truck['end-station'] = int(row[1])
            ride_duration = int(float(row[2]))
            truck['duration'] = ride_duration
            truck['start-date'] = datetime.datetime.utcfromtimestamp(date_sec)
            truck['end-date'] = datetime.datetime.utcfromtimestamp(date_sec+ride_duration)
            new_trucks.append(truck)

    incentives = []
    if len(incentives_str) > 0:
        for row in csv.reader(incentives_str):
            incentive = dict()
            incentive['start-station'] = int(row[0])
            incentive['end-station'] = int(row[1])
            incentive['amount'] = float(row[2])
            incentives.append(incentive)
    
    dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else None
    return json.dumps({'stations-status':stations_status, 'new-bikes':new_bikes, 'new-trucks':new_trucks, 'incentives':incentives}, default=dthandler)

if __name__ == '__main__':
    debug(True)
    run(app, host='localhost', port=8080)
