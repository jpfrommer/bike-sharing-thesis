var r_map;
var pm_map;
var stations = [];
var vehicles = [];
var incentives = [];
var over = false;
var tip;

function init() {
    tip = $("#tip");
	var po = org.polymaps;
    r_map = Raphael($("#map")[0], "100%", "100%");
    var g = po.svg("g");
    $("#map svg").append(g);
	pm_map = po.map().container(g).zoom(13).center({"lat":51.507354, "lon":-0.12723}).add(po.interact());
    pm_map.add(po.image().url(po.url("http://acetate.geoiq.com/tiles/terrain/{Z}/{X}/{Y}.png")));
	pm_map.add(po.compass().pan("none"));

    var stations_url = "http://localhost:8080/stations";
    var j = jQuery.getJSON(stations_url, function(data){
        for(var i=0;i<data.length;i++) 
            stations[data[i]['_id']-1] = new Station(data[i]); 
        
        pm_map.on("move", function() { 
            for(i=0;i<stations.length;i++) {
                if(typeof stations[i] !== 'undefined')
                    stations[i].redraw();
            }
        });
        
        $(document).mousemove(function(e) {
            if (over) 
                $(tip).css("left", e.clientX+20).css("top", e.clientY+20);
        });
        simu_loop();
    });
}

// ========== Bike Viz ========
function Vehicle(data) {
    this.duration = data.duration;
    this['start-time'] = new Date(data['start-date']);
    this['end-time'] = new Date(this['start-time'].getTime() + this.duration*1000);
    
    if(typeof stations[data['start-station']-1] === 'undefined') {
        console.log('station not found');
        console.log(data['start-station']-1);
        return null;
    }

    this['truck'] = data.truck;
    this['start-pos'] = stations[data['start-station']-1].pos;
    if(typeof stations[data['end-station']-1] === 'undefined')
        console.log(data);
    this['end-pos'] = stations[data['end-station']-1].pos;
    this['paper-start'] = pm_map.locationPoint(this['start-pos']);
    this['paper-end'] = pm_map.locationPoint(this['end-pos']);

    var self = this; // for access from closure functions
        
    this.remove = function() {
        this.remove(); // this is the circle
        var idx = vehicles.indexOf(self);
        if(idx !== -1)
            vehicles.splice(idx,1);
    };

    this.redraw = function(curr_time) {
        if(typeof this.circ !== 'undefined') {
            this.circ.remove();
        }
        var perc_finished = (curr_time - this['start-time'])/(this['end-time'] - this['start-time']);
        if(perc_finished >= 1.0)
            return;
        this['paper-start'] = pm_map.locationPoint(this['start-pos']);
        this['paper-end'] = pm_map.locationPoint(this['end-pos']);
        var trans_x = this['paper-end'].x-this['paper-start'].x;
        var trans_y = this['paper-end'].y-this['paper-start'].y;
        this.circ = r_map.circle(this['paper-start'].x + perc_finished*trans_x, this['paper-start'].y + perc_finished*trans_y, 3);
        if(this.truck) {
            this.circ.attr({"stroke-width":0, "fill": "yellow"});
        } else {
            this.circ.attr({"stroke-width":0, "fill": "green"});
        }
        this.circ.animate({transform:"t"+(trans_x*(1-perc_finished))+","+(trans_y*(1-perc_finished))}, this.duration*1000/speedup*(1-perc_finished), null, this.remove);
    };

    return this;
}

// ========== Station Viz ========
function Station(data) {
    this.id = data['_id'];
    this.name = data.name;
    this.pos = data.pos;
    this.map_pos = pm_map.locationPoint(this.pos);
    this.bikes = -1; // get from matlab;
    this.spaces = data.spaces;
    this.broken = data.broken;

    // Draw for the first time 
	this.total_space = this.spaces + data.bikes;
    this.radius = Math.sqrt(this.total_space/Math.PI)*2;
    this.circle = r_map.circle(this.map_pos.x,this.map_pos.y,this.radius);
    this.circle.attr({"fill": "white", "fill-opacity": 20});
    var station = this;
    var tiptext = function() {
        return station.id + ", " + station.name +
            "<br/>bikes: " + station.bikes.toString() +
            "<br/>spaces: " + station.spaces.toString() +
            "<br/>total: "+station.total_space.toString();
    };
    this.circle.mouseover(function() {
        over = true;
        $(tip).html(tiptext());
        $(tip).show();
    });
    this.circle.mouseout(function() {
        $(tip).hide();
        over = false;
    });   
    
    this.redraw = function() {
        this.map_pos = pm_map.locationPoint(this.pos);
        this.circle.attr('cx',this.map_pos.x);
        this.circle.attr('cy',this.map_pos.y);
    };

    this.update_station = function(data) {
        this.bikes = data.bikes;
        this.spaces = data.spaces;
        this.broken = data.broken;
        var blue = parseInt(this.spaces/this.total_space * 254)+1;
        var red = parseInt(this.bikes/this.total_space * 254)+1;
        var c = Raphael.getRGB("rgb("+red.toString() + ", 0, " + blue.toString() + ")");
	    this.circle.attr({"stroke-width":0, "fill": c.hex});
    };
    return this;
}

// ========== Incentives Viz ========
function Incentive(data) {
    this['start-station'] = stations[data['start-station']-1];
    this['end-station'] = stations[data['end-station']-1];
    this.amount = data.amount;

    var self = this; // for access from closure functions
        
    this.remove = function() {
        self.arrow.remove(); // this is the arrow
        var idx = incentives.indexOf(self);
        if(idx !== -1)
            incentive.splice(idx,1);
        delete self;
    };

    this.redraw = function() {
        var start_pos = pm_map.locationPoint(self['start-station'].pos);
        var end_pos = pm_map.locationPoint(self['end-station'].pos);
       
        if(typeof self.arrow !== 'undefined')
            self.arrow.remove();

        self.arrow = r_map.path("M"+start_pos.x+","+start_pos.y+"L"+end_pos.x+","+end_pos.y);
        self.arrow.attr('arrow-end','block-wide-long');
        self.arrow.attr('stroke','black');
        self.arrow.attr('stroke-width',self.amount);
    };

    this.redraw(); // initialize

    return this;
}


function simu_loop() {
    //var time = new Date(1280469601000); //orig start
    var time = new Date(1280469601000 - 10);
    //var time = new Date(1283792900000);
    var interval = 500;
    function redraw_bikes() {
        for(var i=0;i<vehicles.length;i++) {
            if(typeof vehicles[i] !== 'undefined') {
                if(typeof vehicles[i].redraw === 'undefined') {
                    vehicles[i].remove(); }
            }
        }

        for(var i=0;i<incentives.length;i++) {
            if(typeof inentives[i] !== 'undefined')
                incentives[i].redraw();
        }
    }
    pm_map.on("move", redraw_bikes);

    newbikes_url = "";
    var lock = false;
    setInterval (function() {

        if(lock == true)
            return;
        lock = true;

        speedup = parseInt(document.getElementById("speedup").value);
        showbikes = document.getElementById("showbikes").checked;
        showtrucks = document.getElementById("showtrucks").checked;
        
        $("#span_time").text(time.toString());
        var controlparam = "1";
        var newbikes_url = "http://localhost:8080/update_world/" + Math.floor(time.getTime()/1000)+"/"+controlparam;
        n = jQuery.getJSON(newbikes_url, function(data) {
            var inc = data['incentives'];
            if(inc.length > 0) {
                for (var t=0;t<incentives.length;t++) {
                    incentives[t].remove();
                }
            }

            for(var i=0;i<inc.length;i++) {
                if(inc[i].amount <= 0)
                    continue;
                incentives.push(new Incentive(inc[i]));
            }
            
            var status = data['stations-status'];
			for(var i=0;i<status.length;i++) {
                if(stations[i] && stations[i].bikes != status[i].bikes) {
                    stations[i].update_station(status[i]);
                }
            }
            
            if(showbikes) {
                var bikesdata = data['new-bikes'];
				for(var i=0;i<bikesdata.length;i++) {
                    var d = bikesdata[i];
                    d.truck = false;
                    var bike = new Vehicle(d);
                    if(bike) {
                        vehicles.push(bike);
                        bike.redraw(time);
                    }
                }
            }

            if(showtrucks) {
                var truckdata = data['new-trucks'];
				for(var i=0;i<truckdata.length;i++) {
                    var d = truckdata[i];
                    d.truck = true;
                    var truck = new Vehicle(d);
                    if(truck) {
                        vehicles.push(truck);
                        truck.redraw(time);
                    }
                }
            }

        });
        time = new Date(time.getTime()+interval*speedup);

        lock = false;

    }, interval);
}
